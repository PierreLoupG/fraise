;;; File: utils.scm


(define CS "c s")
(define RETADDR "ret addr")

;;;============================================================================
;; utils debug


	(define KRED  "\33[31;1m")
	(define KYEL  "\33[33;1m")
	(define KGRN  "\33[32;1m")
	(define KGRY  "\33[30;1m")
	(define KCYN  "\33[36;1m")
	(define RESET "\33[0m")

	(define-macro (printcpl expr env context)
		`(begin
			(print KGRY "### " KRED "[" context "] " KYEL "COMPILE" RESET " : " KGRN)
			(display ,expr)
			(print RESET "\n")
			(display ,env)
			(println "\n---")))
		
	(define-macro (printlog tag val)
		`(begin (print KGRY "\n### " KRED ,tag RESET " : " KCYN) (display ,val) (println RESET "\n---")))

;;;============================================================================
;; utils compil	

	;; (list-copy list) 
	;; effectue une copie de la liste et renvoie cette dernière
	(define (list-copy list)
		(if (null? list) '() (cons (car list) (list-copy (cdr list)))))

	;; (list-pos list item)
	;; retourne la position d'un élément dans une liste
	(define-macro (list-pos list item)
		`(let ((n (member ,item ,list)))
			(if n
				(- (length ,list) (length n))
				#f)))

	;; (++ var) 
	;; incrémente de 1 la variables var
	(define-macro (++ var)
		`(set! ,var (+ ,var 1)))

	;; (for var lo hi . body) 
	;; boucle for
	(define-macro (for var lo hi . body)
		(let ((H (gensym)) (loop (gensym)))
		  `(let ((,H ,hi))
		     (let ,loop ((,var ,lo))
		       (if (<= ,var ,H)
		           (begin
		             ,@body
		             (,loop (+ ,var 1))))))))

	;; (genlsym n) 
	;; créer une liste de symbole de taille n               
	(define-macro (genlsym n)
		`(let loop ((i 0) (l '()))
			(if (>= i ,n)
				l
				(loop (+ i 1) (cons (gensym) l)))))
					
	;; (genvar expr)
	;; génére est retourne une variable avec comme valeur l'expression		
	(define (genvar expr)
		`(,(gensym) ,expr))
	
	;; (cars l)
	;; retourne le car sans parenthèse, utilisé pour des map sur des listes de pair
	;;
	;; (map cars '((a . 1) (b . 2) (c . 3))) => (a b c)
	(define (cars l)
		`(,@(car l)))
	
	;; (cdrs l)
	;; retourne le cdr sans parenthèse, utilisé pour des map sur des listes de pair
	;;
	;; (map cdrs '((a . 1) (b . 2) (c . 3))) => (1 2 3)
	(define (cdrs l)
		`(,@(cadr l)))	
		
	;; (prefix p var)
	;; ajoute le préfix 'p' au début de la variable p
	;; prend en charge les symbole et les strings
	(define-macro (prefix p var)
		`(if (symbol? ,var)
			(string->symbol (string-append ,p (symbol->string ,var)))
			(string->symbol (string-append ,p ,var))))	
	
	(define-macro (prefix-str p var)
		`(if (symbol? ,var)
			(string-append ,p (symbol->string ,var))
			(string-append ,p ,var)))

	;; (begin* expr1 . exprs) 
	;; exécute expr1 et exprs et renvoie expr1
	(define-macro (begin* expr1 . exprs)
		`(let ((x ,expr1) (t (lambda () ,@exprs))) (t) x))
	
	;; (union a b) fait l'union des deux listes
	;; source : https://stackoverflow.com/questions/42353424/scheme-union-two-lists#42357085
	(define (union a b)
		(cond ((null? b) a)
		      ((member (car b) a)
		       (union a (cdr b)))
		      (else (union (reverse (cons (car b) (reverse a))) (cdr b)))))		
		      
	;; (replace source target replacement) remplace un element
	;; la liste source
	;; source : https://stackoverflow.com/a/18966583/9390440
	(define (replace source target replacement) 
		(cond ((equal? source target) replacement) ;; equal, return replacement
		      ((not (pair? source)) source)        ;; not equal && not pair, return source
		      (else (cons (replace (car source) target replacement) ;; recurse
		                  (replace (cdr source) target replacement)))))	
		                  
	;; (concatenate lists) takes the list of lists "lists" and concatenates
	;; all the lists.  For example:
	;;
	;; (concatenate '( (1 2) (3) (4 5 6) )) => (1 2 3 4 5 6)
	(define (concatenate lists)
		(fold-right append '() lists))   
		                 
;;;============================================================================
;; test	variable d'environnement

	;; (glo? v)
	;; retourne si la variable est globale
	(define-macro (glo? v)
		`(= (cdr ,v) 0))

	;; (close? v)
	;; retourne si la variable est un indicateur de fermeture
	(define-macro (close? v)
		`(eq? (car ,v) #f))

	;; (free? v)
	;; retourne si la variable est considéré comme libre
	(define-macro (free? v)
		`(if (eq? ,v #f) #f (< (cdr ,v) 0)))
		
	;; (assig? v)
	;; retourne si la variable est assigné
	;; prend en charge les pairs et les noms string / symboles directement
	(define-macro (assig? v)
		`(if (pair? ,v)
			(if (symbol? (car ,v))			
				(if (< (string-length (symbol->string (car ,v))) 3)
					#f
					(string=? (substring (symbol->string (car ,v)) 0 3) "#f_"))
				(if (< (string-length (car ,v)) 3)
					#f
					(string=? (substring (car ,v) 0 3) "#f_")))
			(if (< (string-length (symbol->string ,v)) 3)
				#f
				(string=? (substring (symbol->string ,v) 0 3) "#f_"))))

	;; (env?assig env v)
	;; retourne si 'v' a une variable assignée dans l'environnement
	(define-macro (env?assig env v)
		`(env?var ,env (prefix "#f_" ,v)))	
			
;;;============================================================================
;; opération sur environnement	
	
	;; (env-sp+ env) 
	;; incrémente le stack pointeur de 1
	(define-macro (env-sp+ env)
			`(begin
				(set-car! ,env (+ (car ,env) 1))
				,env))

	;; (env-sp! env sp) 
	;; définit la valeur du stack pointer de l'environnement et le retourne
	(define-macro (env-sp! env sp)
		`(begin
			(set-car! ,env ,sp)
			,env))

	;; (env-sp+! env val)
	;; incrémente le stack pointeur de 'val'
	(define-macro (env-sp+! env val)
		`(begin
			(set-car! ,env (+ (car ,env) ,val))
			,env))

	;; (env+var env var) 
	;; ajoute la variable var dans l'environnement et le retourne
	(define-macro (env+var env var)
		`(begin 
			(set-cdr! ,env (cons (cons (symbol->string ,var) (car ,env)) (cdr ,env)))
			,env))

	(define (env+marker env marker)
		(begin
			(set-cdr! env (cons (cons marker (car env)) (cdr env)))
			env))

	;; (env+close env) 
	;; ajoute un indicateur de fermeture
	;; à l'environnement de valeur #f car aucune variable
	;; ne pourra porter se nom. Cette indicateur permet de
	;; de signaler qu'une fermeture se trouve à cette endroit
	;; sur la pile
	(define (env+cs env) (env+marker env CS))
	(define (env+close env) (env+marker (env-sp+ env) #f))
	(define (env+retaddr env)    (env+marker (env-sp+ env) RETADDR))

	;; (env+vars env vars) 
	;; ajoute plusieurs variables à
	;; l'environnement et renvoie ce dernier à jour
	(define (env+vars env vars)
		(if (null? vars)
			env
			(begin
				(env-sp+ env)
				(env+vars (env+var env (car vars)) (cdr vars))))) 
			
	;; (env+glo env name) 
	;; ajoute la variable globale dans l'environnement à la position 0
	(define-macro (env+glo env name)
		`(begin 
			(set-cdr! ,env (reverse (cons (cons (symbol->string ,name) 0) (reverse (cdr ,env)))))
			,env))
	
	(define (env-marker-pos env marker)
		(- (car env) (cdr (assoc marker (cdr env)))))
		
	;; (env-close-pos env) 
	;; retourne la position de l'indicateur de fermeture le plus proche
	(define (env-cs-pos env)   (env-marker-pos env CS))
	(define (env-close-pos env)   (env-marker-pos env #f))
	(define (env-retaddr-pos env) (env-marker-pos env RETADDR))
	
	(define-macro (cs? v)
	  `(equal? (car ,v) CS))
	  

	;; (env?var env var) 
	;; retourne la variable var de l'environnement
	(define-macro (env?var env var)
		`(assoc (symbol->string ,var) (cdr ,env)))
					
	;; (env-var-pos env var) 
	;; retourne la position d'une variable dans son environnement
	(define-macro (env-var-pos env var)
		`(- (car ,env) (cdr ,var)))	
		
	;; (env-set-free env) 
	;; place toutes les variables non globales
	;; et non free à l'état free en mettant -1 à leur position
	;; et supprime les indicateurs de fermeture
	(define (env-set-free env)
		(let ((e (cons '0 '())))
			(set-cdr! env (cdr env))
			(for i 0 (- (length (cdr env)) 1)
				(let ((v (list-ref (cdr env) i)))
					(cond ((close? v) '())
								((or (glo? v) (free? v) (cs? v)) (set-cdr! e (cons v (cdr e))))
								(else (set-cdr! e (cons (cons (car v) -1) (cdr e)))))))
			(set-cdr! e (reverse (cdr e)))
			e))

	;; (env-get-free env) 
	;; retourne toutes les variables libre de l'environnement
	(define (env-get-free env)
		(let ((f '()))
			(for i 0 (- (length (cdr env)) 1)
				(let ((v (list-ref (cdr env) i)))
					(if (free? v)
						(set! f (cons (string->symbol (car v)) f)))))
			(reverse f)))
	
	;; (env-get-glo env)
	;; retourne la liste des globabes de l'environnement
	(define (env-get-glo env)
		(let ((g '()))
			(for i 0 (- (length (cdr env)) 1)
				(let ((v (list-ref (cdr env) i)))
					(if (and (glo? v) (not (cs? v)))
						(set! g (cons v g)))))
			g))
			
	;; (union-glo env glo)
	;; fait l'union de l'environnement 'env' et des globales 'glo'
	(define-macro (union-glo env glo)
		`(cons (car ,env) (union (cdr ,env) ,glo)))

;;;============================================================================
;; assignation
			
	;; (get-assign vars)
	;; génére les assignations pour les variables vars
	;; ne retourne pas les variables déjà assignées
	;;
	;; (get-assig '(a b)) => ((#f_a ($cons a #f)) (#f_b ($cons b #f)))
	(define (get-assig vars)
		(if (null? vars)
			'()
			(let ((assig '()))
				(for i 0 (- (length vars) 1)
					(let ((v (list-ref vars i)))
						(if (not (assig? v))
							(set! assig (append
													`((,(prefix "#f_" v) ($cons ,v #f)))
							 						assig)))))
			assig)))
	
	;; (get-let-assign vars)
	;; génére les assignations pour les variables dun let
	;; retourne les variables déjà assignées
	(define (get-let-assig vars)
		(if (null? vars)
			'()
			(let ((assig '()))
				(for i 0 (- (length vars) 1)
					(let ((v (list-ref vars i)))
						(if (not (assig? v))
							(set! assig (append
													`((,(prefix "#f_" (car v)) ($cons ,(cadr v) #f)))
							 						assig))
							(set! assig (append `(,v) assig)))))
			(reverse assig))))
	
	;; (get-env-assign env)
	;; généré les assignations pour les variables de l'environnement
	;; qui ne sont libre, non assignées et qu'il n'existe pas de variable
	;; assignées du même nom
	(define (get-env-assig env)
		(let ((assig '()) (e (cdr env)))
			(for i 0 (- (length e) 1)
				(let ((v (list-ref e i)))
					(if (and (free? v) (not (assig? v)) (not (env?assig env (car v))))
						(set! assig (append
												`((,(prefix "#f_" (car v)) ($cons ,(string->symbol (car v)) #f)))
						 						assig)))))
				assig))

;;;============================================================================
;; letrec transformation

	;; (letrec-gen-bindings v) génère l'instancation des variables du letrec
	(define (letrec-gen-bindings v)
		`(,(car v) ($cons #f '())))

	;; (letrec-gen-body a b) génère la création des lambda
	(define (letrec-gen-lam a b)
		`($set-car! ,(car a) ,(cadr b)))

	;; (letrec-id bindings) retourne la variable du bindings
	(define (letrec-id bindings)
		`(,(car bindings)))

	;; (letrec-gen-link expr) remplace chaque nom de variable par
	;; ($car <nom de la variable>)				
	(define (letrec-gen-link expr)
		(let loop ((id (map letrec-id expr)))
			(if (null? id)
				expr
				(begin
					(set! expr (replace expr (caar id) `($car ,(caar id))))
					(loop (cdr id))))))
  
  (define (letrec-gen-body body vars)
    (let loop ((v vars))
      (if (null? v)
        body
        (begin
          (set! body (replace body (caar v) `($car ,(caar v))))
          (loop (cdr v))))))

	;; (letrec-gen bindings body) génère le code source à source
	;; du letrec
	(define (letrec-gen bindings body)
		(let* ((v (map letrec-id bindings))
		      (a (map letrec-gen-bindings bindings))
					(b (map letrec-gen-lam v (letrec-gen-link bindings)))
					(c (letrec-gen-body body v)))
					;(display c)
					`(let ,a ,@b ,@c)))

;;;============================================================================
;; data

	;; (data-merge data1 data2) 
	;; fusionne data2 à data1
	;; - fusionne les IR des deux data dans le bon ordre
	;; - l'environnement de data1 est remplacé par celui à jour de data2
	;; - met à jour la taille du stack de data1 par celle de data2
	(define-macro (data-merge data1 data2)
		`(begin
			(set-car! ,data1 (append (car ,data1) (car ,data2))) 
			(set-cdr! (cdr ,data1) (append (list (caddr ,data2)) (cddr ,data1)))
			(set-car! (cdr ,data1) (cadr ,data2))))
			
	;; (dup-env data)
	;; duplique et retourne l'environnement de data
	(define-macro (dup-env data)
		`(list-copy (cdr ,data)))

	;; (make-data env . ir)
	;; simplifie la compilation d'une opération
	;; terminale, elle regroupe l'environnement et l'instruction
	(define-macro (make-data env . ir)
		`(cons (append ,@ir) ,env))
	
	(define (send env context)
	  (if (eq? context 'tail)
        (begin
          ;(printlog "RET" env)
          `((ret ,(+ (env-retaddr-pos env) 0) ,(- (env-cs-pos env) 1))))
        `()))
	
	;; (make-data-aux ir env exprs)
	;; compile les 'exprs' en faisant succèder les environnements
	;; ajoute l'instructions 'ir' et cons avec l'environnement
	;; retourne le data finale après la compilations des 'exprs'
	(define-macro (make-data-aux ir env context exprs)
		(let ((irs (gensym)) (fdata (gensym)) (ldata (gensym))
				  (loop (gensym)) (ex (gensym)) (ev (gensym)) (data (gensym)))
			`(let ((,irs '()) (,fdata '()) (,ldata '()))
				(let ,loop ((,ex ,exprs) (,ev ,env))
					(if (null? ,ex)
						(cons
							(append 
							  ,irs 
							  (if (list? ,ir) ,ir (list (list ,ir)))
							  (send (cdr ,fdata) context))
							(env-sp! (cdr ,ldata) (cadr ,fdata)))
						(let ((,data (compile-expr (car ,ex) (list-copy ,ev) 'push)))
							(if (null? ,fdata) (set! ,fdata ,data))
							(set! ,ldata ,data)
							(set! ,irs (append ,irs (car ,data)))
							(,loop (cdr ,ex) (cdr ,data))))))))
		
	;; (make-data* ir env . exprs)
	;; transfert vers la fonction auxilliaire pour que 'exprs' soit
	;; considéré comme une liste et non une procédure par le `				
	(define (make-data* ir env context . exprs)
		(make-data-aux ir env context exprs))	
	
	;; (env-close env) 
	;; renvoie les instructions pour la création de
	;; la fermeture. La fermture doit contenir les variables libres
	;; de la fermeture courante plus les variables de l'environnement
	(define (env-close env)
		(begin
			(let*
				; copie de l'env + 1 sur sp pour simuler le 'push_proc' pour la fermeture
				((e (list-copy (env-sp+ env)))
				; compteur de variable placée dans la fermeture
				(c 0)
				; ensemble des variables free de l'env
				(free (env-get-free e))
				; instructions finales
				(ir '()))
					(for i 0 (- (length (cdr e)) 1)
						(let ((v (list-ref (cdr e) i)))
							(if (and (not (glo? v)) (not (close? v)) (not (cs? v)))
								(begin
									(++ c)
									(if (free? v)
										(set! ir (append
											; l'ordre des instructions sera inversé, on inverse l'ordre aussi ici
											(list (list 'push_free (list-pos free (string->symbol (car v)))))
											(list (list 'push_loc (env-close-pos e)))
											ir))
										(set! ir (append 
											(list (list 'push_loc (- (car e) (cdr v))))
											ir)))
									(env-sp+ e)))))
					(append
						(reverse ir)
						`((close ,c))))))
						
						
						
