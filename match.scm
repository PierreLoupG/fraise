;;; File: match.scm

;;; This file defines the match macro which implements a pattern matcher.

;;;============================================================================

;; Macros for controlling the evaluation time.

(define-macro (for-macro-expansion . body)
  (eval `(begin ,@body))
  #f)

(define-macro (for-macro-expansion-and-execution . body)
  `(begin
     (for-macro-expansion ,@body)
     ,@body))

;;;============================================================================

;; The match macro.

(for-macro-expansion-and-execution

  (define-macro (match subject . clauses)

    (define (if-equal? var pattern yes no)
      (cond ((and (pair? pattern)
                  (eq? (car pattern) 'unquote)
                  (pair? (cdr pattern))
                  (null? (cddr pattern)))
             `(let ((,(cadr pattern) ,var))
                ,yes))
            ((null? pattern)
             `(if (null? ,var) ,yes ,no))
            ((symbol? pattern)
             `(if (eq? ,var ',pattern) ,yes ,no))
            ((boolean? pattern)
             `(if (eq? ,var ,pattern) ,yes ,no))
            ((or (number? pattern)
                 (char? pattern))
             `(if (eqv? ,var ,pattern) ,yes ,no))
            ((string? pattern)
             `(if (equal? ,var ,pattern) ,yes ,no))
            ((pair? pattern)
             (let ((carvar (gensym))
                   (cdrvar (gensym)))
               `(if (pair? ,var)
                    (let ((,carvar (car ,var)))
                      ,(if-equal?
                        carvar
                        (car pattern)
                        `(let ((,cdrvar (cdr ,var)))
                           ,(if-equal?
                             cdrvar
                             (cdr pattern)
                             yes
                             no))
                        no))
                    ,no)))
            (else
             (error "unknown pattern"))))

    (let* ((var
            (gensym))
           (fns
            (map (lambda (x) (gensym))
                 clauses))
           (err
            (gensym)))
      `(let ((,var ,subject))
         ,@(map (lambda (fn1 fn2 clause)
                  `(define (,fn1)
                     ,(if-equal? var
                                 (car clause)
                                 (if (and (eq? (cadr clause) 'when)
                                          (pair? (cddr clause)))
                                     `(if ,(caddr clause)
                                          ,(cadddr clause)
                                          (,fn2))
                                     (cadr clause))
                                 `(,fn2))))
                fns
                (append (cdr fns) (list err))
                clauses)
         (define (,err) (error "match failed" ,subject))
         (,(car fns)))))

  (define gensym ;; a version of gensym useful for debugging
    (let ((count 0))
      (lambda ()
        (set! count (+ count 1))
        (string->symbol (string-append "g" (number->string count))))))

)

;;;============================================================================
