;;; File: parser.scm

;;;============================================================================

;; Source code parser.

(define (parse-program filename)
  (with-input-from-file
      filename
    (lambda ()
      (parse-exprs))))

(define (parse-exprs)
  (let ((x (parse-expr)))
    (if (eof-object? x)
        '()
        (cons x
              (parse-exprs)))))

(define (parse-expr)
  (read*)) ;; Just call read to parse S-expression

;;;============================================================================
