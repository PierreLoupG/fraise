;;; File: lib.scm

;;;============================================================================
;;Une librairie contenant plusieurs primitives et fonctions prédéfinies

;;opérations qui nous ont été conférées par la grâce de Mr. Feeley
;;opérations sur les booléens

(define $boolean?
    (lambda (x)
        (or
            ($eq? #t x)
            ($eq? #f x))))

;;opérations d'équivalence

(define $eqv?
  (lambda (obj1 obj2)
    (if ($number? obj1)
        (and ($number? obj2)
             ($= obj1 obj2))
        ($eq? obj1 obj2))))

;;opérations sur les caractères

(define $char-compare
  (lambda (c1 c2)
    (let ((n1 ($char->integer c1))
          (n2 ($char->integer c2)))
      (if ($< n1 n2)
          -1
          (if ($> n1 n2)
              +1
              0)))))

(define $char=?  (lambda (char1 char2) ($=  ($char-compare char1 char2) 0)))
(define $char<?  (lambda (char1 char2) ($<  ($char-compare char1 char2) 0)))
(define $char>?  (lambda (char1 char2) ($>  ($char-compare char1 char2) 0)))
(define $char<=? (lambda (char1 char2) ($<= ($char-compare char1 char2) 0)))
(define $char>=? (lambda (char1 char2) ($>= ($char-compare char1 char2) 0)))

;;opérations sur les strings

(define $string-compare-aux
  (lambda (str1 str2 i len)
    (if ($< i len)
        (let ((c1 ($string-ref str1 i))
              (c2 ($string-ref str2 i)))
          (if ($char<? c1 c2)
              -1
              (if ($char>? c1 c2)
                  +1
                  ($string-compare-aux str1 str2 ($+ i 1) len))))
        (let ((len1 ($string-length str1))
              (len2 ($string-length str2)))
          (if ($< len1 len2)
              -1
              (if ($> len1 len2)
                  +1
                  0))))))

(define $string-compare
  (lambda (str1 str2)
    (if ($eq? str1 str2)
        0
        ($string-compare-aux
         str1
         str2
         0
         (let ((len1 ($string-length str1))
               (len2 ($string-length str2)))
           (if ($< len1 len2) len1 len2))))))

(define $string-append
    (lambda (s1 s2)
        (if (and ($string? s1) ($string? s2))
            (let ((lst1 ($string->list s1)) (lst2 ($string->list s2)))
                (let ((lst ($append lst1 lst2)))
                    ($list->string lst)))
            #f)))
            
(define $substring
    (lambda (s start end)
        (if ($string? s) 
            (let ((l ($string-length s)))
                (if (and ($<= 0 start) (and ($<= start end) ($<= end l)))
                    (let $substring-loop ((s2 ($make-string ($- end start) #\x)) (i 0))
                        (if ($< ($+ i start) end)
                            (begin
                                ($string-set! s2 i ($string-ref s ($+ i start)))
                                ($substring-loop s2 ($+ 1 i)))
                            s2))
                    #f))
            #f)))

(define $string=?  (lambda (str1 str2) ($=  ($string-compare str1 str2) 0)))
(define $string<?  (lambda (str1 str2) ($<  ($string-compare str1 str2) 0)))
(define $string>?  (lambda (str1 str2) ($>  ($string-compare str1 str2) 0)))
(define $string<=? (lambda (str1 str2) ($<= ($string-compare str1 str2) 0)))
(define $string>=? (lambda (str1 str2) ($>= ($string-compare str1 str2) 0)))

;;opérations sur les vectors


(define $equal?
  (lambda (obj1 obj2)
    (if ($number? obj1)
        (and ($number? obj2)
             ($= obj1 obj2))
        (if ($pair? obj1)
            (and ($pair? obj2)
                 (and ($equal? ($car obj1) ($car obj2))
                    ($equal? ($cdr obj1) ($cdr obj2))))
            (if ($vector? obj1)
                (and ($vector? obj2)
                     ($vector=? obj1 obj2))
                (if ($string? obj1)
                    (and ($string? obj2)
                         ($string=? obj1 obj2))
                    ($eq? obj1 obj2)))))))

(define $vector=?-aux
  (lambda (vec1 vec2 i)
    (or ($< i 0)
        (let ((elem1 ($vector-ref vec1 i))
              (elem2 ($vector-ref vec2 i)))
          (and ($equal? elem1 elem2)
               ($vector=?-aux vec1 vec2 ($- i 1)))))))
               
(define $vector=?
  (lambda (vec1 vec2)
    (let ((len1 ($vector-length vec1))
          (len2 ($vector-length vec2)))
      (and ($= len1 len2)
           ($vector=?-aux vec1 vec2 ($- len1 1))))))

;;opérations sur les listes

(define $null?
    (lambda (x)
        ($eq? x '())))

(define $member
  (lambda (x lst)
    (and ($pair? lst)
         (if ($equal? x ($car lst))
             lst
             ($member x ($cdr lst))))))

(define $assoc
  (lambda (key alist)
    (and ($pair? alist)
         (let ((x ($car alist)))
           (if ($equal? key ($car x))
               x
               ($assoc key ($cdr alist)))))))
               
(define $append
    (lambda (lst1 lst2)
        (if (and ($pair? lst1) ($pair? lst2))
            (let $a-loop ((lst lst1) (l1 lst1) (l2 lst2))
                (if ($eq? ($cdr l1) '())
                    (begin 
                        ($set-cdr! l1 l2)
                        lst)
                    ($a-loop lst ($cdr l1) l2)))
            #f)))
                
(define $reverse 
    (lambda (lst)
        (and ($pair? lst)
            (let $r-loop ((lst1 lst) (lst2 '()))
                (if ($eq? '() lst1)
                    lst2
                    ($r-loop ($cdr lst1) ($cons ($car lst1) lst2)))))))

(define $length
    (lambda (lst)
        (and ($pair? lst)
            (let $ll-loop ((l lst) (n 0))
                (if ($eq? '() l)
                    n
                    ($ll-loop ($cdr l) ($+ n 1)))))))
                    
(define $map
    (lambda (f lst)
        (and ($pair? lst)
            (if ($eq? '() ($cdr lst))
                ($cons (f ($car lst)) '())
                ($cons (f ($car lst)) ($map f ($cdr lst)))))))
                
               
;;opérations de conversion
;;number->string

(define $number->string-aux
  (lambda (n i extra)
    (if ($= n 0)
        ($make-string ($+ i extra) #\-)
        (let ((str ($number->string-aux ($quotient n 10) ($+ i 1) extra)))
          ($string-set! str
                        ($- ($string-length str) ($+ i 1))
                        ($integer->char ($- 48 ($remainder n 10))))
          str))))

(define $number->string
  (lambda (n)
    (if ($< n 0)
        ($number->string-aux n 0 1)
        (if ($> n 0)
            ($number->string-aux ($- 0 n) 0 0)
            ($make-string 1 #\0)))))

;;string->number

(define $string->number-aux2
  (lambda (str i n)
    (if ($< i ($string-length str))
        (let ((d ($char->integer ($string-ref str i))))
          (and ($>= d 48)
               (and($<= d 57)
                    ($string->number-aux2 str ($+ i 1) ($- ($* 10 n) ($- d 48))))))
        n)))

(define $string->number-aux
  (lambda (str i)
    (and ($< i ($string-length str))
         ($string->number-aux2 str i 0))))

(define $string->number
  (lambda (str)
    (and ($> ($string-length str) 0)
         (let ((c ($string-ref str 0)))
           (let ((n ($string->number-aux
                     str
                     (if (or ($char=? c #\+) ($char=? c #\-)) 1 0))))
             (and n
                  (if ($char=? c #\-) n ($- 0 n))))))))

;;string->list

(define $string->list
    (lambda (x)
        (if ($string? x)
            (let ((lst ($cons ($string-ref x 0) '())))
                (let $string->list-loop ((lst1 lst) (lst2 lst) (l ($string-length x)) (n 1))
                    (if ($< n l)
                        (begin 
                            ($set-cdr! lst2 ($cons ($string-ref x n) '()))
                            ($string->list-loop lst1 ($cdr lst2) l ($+ 1 n)))
                        lst1)))
            #f)))

;;list->string

(define $list->string
    (lambda (x)
        (if ($pair? x)
            (let ((l ($length x)))
                (let $list->string-loop ((s ($make-string l #\x)) (lst x) (n 0))
                    (if ($char? ($car lst))
                        (if ($null? ($cdr lst))
                            (begin 
                                ($string-set! s n ($car lst))
                                s)
                            (begin 
                                ($string-set! s n ($car lst))
                                ($list->string-loop s ($cdr lst) ($+ 1 n))))
                    #f)))
            #f)))


;;list->vector
            
(define $list->vector
    (lambda (x)
        (if ($pair? x)
            (let ((l ($length x)))
                (let $list->vector-loop ((v ($make-vector l 0)) (lst x) (n 0))
                    (if ($null? ($cdr lst))
                        (begin 
                            ($vector-set! v n ($car lst))
                            v)
                        (begin 
                            ($vector-set! v n ($car lst))
                            ($list->vector-loop v ($cdr lst) ($+ 1 n))))))
            #f)))
            
;;string->symbol

(define $sym-table ($cons 0 '()))

(define $string->symbol
    (lambda (x)
        (if ($string? x)
            (let ((result ($assoc x ($cdr $sym-table))))
                (if result
                    ($string->symbolaux ($car result))
                    (begin
                        ($set-car! $sym-table ($+ 1 ($car $sym-table)))
                        ($set-cdr! $sym-table ($cons ($cons x ($car $sym-table)) ($cdr $sym-table)))
                        ($string->symbolaux x))))
            #f)))
    
;;opérations de input

(define $buf #f) ;; input buffer, #f means buffer empty or end of file reached

(define $peek-char
    (lambda ()
        (or $buf 
            (let ((x ($read-u8)))
                (set! $buf (and ($>= x 0) ($integer->char x)))
                $buf))))
               
(define $read-char
    (lambda ()
        (set! $buf #f)
        ($peek-char)))
                                
(define ($peek-char-non-whitespace)
  (let ((c ($peek-char)))
    (if (or ($eq? c #f)
            ($char>? c #\space))
        c
        (begin
          ($read-char)
          ($peek-char-non-whitespace)))))

(define ($read-single-char)
	(let ((c ($peek-char)))
		(if (or ($eq? c #f)
					(or ($char=? c #\newline)
					    ($char<=? c #\space)))
				'()
				(begin
					($read-char)
					($cons c ($read-single-char))))))

(define ($read-symbol)
  (let ((c ($peek-char)))
    (if (or ($eq? c #f)
            (or ($char=? c #\u0028)
                (or($char=? c #\u0029)
                    ($char<=? c #\space))))
        '()
        (begin
          ($read-char)
          ($cons c ($read-symbol))))))

(define ($read-comment) ;;lit un commentaire jusqu'à un end of file ou \n et retourne nil
  (let ((c ($peek-char)))
    (if (or ($eq? c #f)
            ($char=? c #\newline))
        '()
        (begin
          ($read-char)
          ($read-comment)))))

(define ($read-string)
	(let ((c ($peek-char)))
		(if (or ($eq? c #f)
					(or ($char=? c #\u0022)
						($char=? c #\newline)))
				(begin
					($read-char) ;; skip " (sinon le " fermant sera considéré comme un ouvrant)
					'())
				
				(if ($char=? c #\\) ;; si on lit un charactère échappé
				    (begin
				    ($read-char)
				    (if (or ($char=? ($peek-char) #\u0022) ($char=? ($peek-char) #\\) 
				            ($char=? ($peek-char) #\|) ($char=? ($peek-char) #\?)) ;; ", \, | ou ? échappé
				      ($cons ($read-char) ($read-string))
				      (if ($char=? ($peek-char) #\n) ;; un saut de ligne échappé
				        (begin 
				          ($read-char)
				          ($cons #\newline ($read-string)))
			            (if ($char=? ($peek-char) #\a)
			              (begin 
				            ($read-char)
				            ($cons #\u0007 ($read-string)))
				          (if ($char=? ($peek-char) #\b)
			                (begin 
				              ($read-char)
				              ($cons #\u0008 ($read-string)))
				            (if ($char=? ($peek-char) #\t)
			                  (begin 
				                ($read-char)
				                ($cons #\u0009 ($read-string)))
				              (if ($char=? ($peek-char) #\v)
			                    (begin 
				                  ($read-char)
				                  ($cons #\u000b ($read-string)))
				                (if ($char=? ($peek-char) #\f)
			                      (begin 
				                    ($read-char)
				                    ($cons #\u000c ($read-string)))
				                  (if ($char=? ($peek-char) #\r)
			                        (begin 
				                      ($read-char)
				                      ($cons #\u000d ($read-string))))))))))))			    
				    
				    (begin
					 ($read-char)
					 ($cons c ($read-string)))))))

(define ($read-list)
  (let ((c ($peek-char-non-whitespace)))
    (if ($char=? c #\u0029)
        (begin
          ($read-char) ;; skip ")"
          '())
        ;; si la liste n'est pas finie on recommence la lecture 
        ;; pour d'éventuelle listes imbriquées
        (if ($char=? c #\.)
            (begin 
                ($read-char)
                (let ((input ($read-list)))
                    (if ($null? ($cdr input))
                        ($car input)
                        ($error "ill-formed improper list: more than 1 element after dot: " input))))
            (let ((first ($read)))
                (let ((rest ($read-list)))
                    ($cons first rest)))))))

(define ($read-hashtag)
  (let ((c ($peek-char)))
    (cond
    		;; boolean true
    		(($char=? c #\t) (begin ($read-char) #t))
    		;; boolean false
    		(($char=? c #\f) (begin ($read-char) #f))
    		;; vector
    		(($char=? c #\u0028) (begin ($read-char) ($list->vector($read-list))))
    		;; caractère unique
    		(($char=? c #\\)
    			($read-char) ;; skip "\"
    			(let ((r ($list->string ($cons ($read-char) ($read-symbol))))) ;; r = le caractère (ou la valeur unicode)
    				(cond
    				        				    
    				    (($eq? 1 ($string-length r)) ($string-ref r 0)) ;;1 caractère, ex: x -> #\x
    				    
    				    ((and ($char=? #\u ($string-ref r 0)) ($> 6 ($string-length r))) ($integer->char ($string->number ($string-append "#x" ($substring r 1 ($string-length r)))))) ;;représentation hex du char, 4 digits
    				    
    				    ((and ($char=? #\U ($string-ref r 0)) ($> 10 ($string-length r))) ($integer->char ($string->number ($string-append "#x" ($substring r 1 ($string-length r)))))) ;;représentation hex du char, 8 digits
    				    
    				    ((or ($string=? "nul" r) ($string=? "null" r)) #\null)
    				    
    				    (($string=? "backspace" r) #\backspace)
    				    
    				    (($string=? "tab" r) #\tab)
    				    
    				    ((or ($string=? "newline" r) ($string=? "linefeed" r)) #\newline)
    				    
    				    (($string=? "vtab" r) #\vtab)
    				    
    				    (($string=? "page" r) #\page)
    				    
    				    (($string=? "return" r) #\return)
    				    
    				    (($string=? "space" r) #\space)
    				)))
    		;; sinon liste vide
    		(else '()))))


(define ($read)
  (let ((c ($peek-char-non-whitespace)))
    (cond 
    	  (($eq? c #f) c)
          ;; caractère "(" = lit et renvoie la liste
          (($char=? c #\u0028)
           	(begin
           	    ($read-char) ;; skip "("
           	    ($read-list)))
          ;; caractère "'" = lit et renvoie la liste sans l'évaluer
          (($char=? c #\')
           	(begin
           	    ($read-char)
           	    ($cons 'quote ($read)))) 
          ;; caractère "#" = traitement des boolean ou chars
          (($char=? c #\#)
           	(begin
           	    ($read-char)
          	    ($read-hashtag)))
          ;; caractère " = traitement d'un string
          (($char=? c #\u0022)
          	(begin
          	    ($read-char)
          	    ($list->string($read-string))))
          ;; caractère ";" = lit le commentaire sans le renvoyer
          (($char=? c #\u003b)
            (begin
                ($read-char) ;; traitement des commentaires
                ($read-comment)
                ($read))) ;; recommence la lecture = saute le commentaire sans rien retourner
          (else
           	(begin
           	    ($read-char) ;; skip first char
           	    (let ((s ($list->string ($cons c ($read-symbol)))))
           		    (or ($string->number s)
              	    ($string->symbol s))))))))
    
;;opérations de output

(define $write-char
  (lambda (c)
    ($write-u8 ($char->integer c))))

(define $newline
  (lambda ()
    ($write-char ($integer->char 10))))

(define $display-boolean
  (lambda (b)
    ($write-char #\#)
    ($write-char (if b #\t #\f))))

(define $display-null
  (lambda ()
    ($write-char #\u0028)
    ($write-char #\u0029)))

(define $display-string-aux
  (lambda (str i)
    (if ($< i ($string-length str))
        (begin
          ($write-char ($string-ref str i))
          ($display-string-aux str ($+ i 1)))
        #f)))

(define $display-string
  (lambda (str)
    ($display-string-aux str 0)))

(define $display-inside-pair
    (lambda (pair)
        ($display ($car pair))
        (if ($null? ($cdr pair))
            ($write-char #\null)
            (begin
                ($write-char #\space)
                (if ($pair? ($cdr pair))
                    ($display-inside-pair ($cdr pair))
                    (begin
                        ($write-char #\.)
                        ($display ($cdr pair))))))))

(define $display
  (lambda (x)
    (if ($eq? x #f)
        ($display-boolean x)
        (if ($eq? x #t)
            ($display-boolean x)
            (if ($eq? x '())
                ($display-null)
                (if ($number? x)
                    ($display-string ($number->string x))
                    (if ($char? x)
                        ($write-char x)
                        (if ($string? x)
                            ($display-string x)
                            (if ($symbol? x)
                                ($display-string ($symbol->string x))
                                (if ($pair? x)
                                    (begin
                                        ($write-char #\u0028)
                                        ($display-inside-pair x)
                                        ($write-char #\u0029))
                                    (if ($vector? x)
                                        (let ()                                        
                                            ($write-char #\#)
                                            ($write-char #\u0028)
                                            (let $display-vector-loop ((i 0) (n ($vector-length x)))
                                                (if ($< i n)
                                                    (begin 
                                                        ($display ($vector-ref x i))
                                                        ($display-vector-loop ($+ 1 i) n))
                                                    ($write-char #\u0029))))
                                        (if ($procedure? x)
                                            ($display-string "<procedure>")
                                            ($display-string "<unknown>")))))))))))))

(define $write-string-aux
  (lambda (str i)
    (if ($< i ($string-length str))
        (let ((c ($string-ref str i)))
            (if (or ($char=? c #\u0022) ($char=? c #\\))
                (begin 
                    ($write-char #\\)
                    ($write-char c))
                ($write-char c))
            ($write-string-aux str ($+ i 1)))
        #f)))

(define $write-string
  (lambda (str)
    ($write-string-aux str 0)))

(define $write
  (lambda (x)
    (if ($eq? x #f)
        ($display-boolean x)
        (if ($eq? x #t)
            ($display-boolean x)
            (if ($eq? x '())
                ($display-null)
                (if ($number? x)
                    ($display-string ($number->string x))
                    (if ($char? x)
                        (begin
                            ($write-char #\#)
                            ($write-char #\\)
                            ($write-char x))
                        (if ($string? x)
                            (begin
                                ($write-char #\u0022)
                                ($write-string x)
                                ($write-char #\u0022))
                            (if ($symbol? x)
                                ($write-string ($symbol->string x))
                                (if ($pair? x)
                                    (begin
                                        ($write-char #\u0028)
                                        ($display-inside-pair x)
                                        ($write-char #\u0029))
                                    (if ($vector? x)
                                        (let ()
                                            ($write-char #\#)
                                            ($write-char #\u0028)
                                            (let $write-vector-loop ((i  0) (n ($vector-length x)))
                                                (if ($< i n)
                                                    (begin 
                                                        ($write ($vector-ref x i))
                                                        ($write-vector-loop ($+ 1 i) n))
                                                    ($write-char #\u0029))))
                                        (if ($procedure? x)
                                            ($display-string "<procedure>")
                                            ($display-string "<unknown>")))))))))))))

(define $println
  (lambda (x)
    ($display x)
    ($newline)))

;;contrôle

(define ($force x) (x))

(define $error 
    (lambda (msg y) 
        ($display msg)
        ($display y)
        ($newline)
        ($halt)))
        
(define $halt (lambda () ($halt)))

;;génération d'identifiants uniques

(define $gensym-count 0)

(define $gensym
  (lambda ()
    (set! $gensym-count ($+ $gensym-count 1))
    ($string->symbol
     ($string-append "_g" ($number->string $gensym-count)))))
        
;;fonctions prédéfinies avec des variables globales

;;arithmétique
(define +           		(lambda (x y) ($+ x y)))
(define -           		(lambda (x y) ($- x y)))
(define *           		(lambda (x y) ($* x y)))
(define quotient    		(lambda (x y) ($quotient x y)))
(define remainder   		(lambda (x y) ($remainder x y)))
(define modulo      		(lambda (x y) ($modulo x y)))

;;logique
(define =           		(lambda (x y) ($= x y)))
(define >           		(lambda (x y) ($> x y)))
(define >=          		(lambda (x y) ($>= x y)))
(define <           		(lambda (x y) ($< x y)))
(define <=          		(lambda (x y) ($<= x y)))
(define not         		(lambda (x)   ($not x)))

;;booléens
(define boolean?    		(lambda (x)   ($boolean? x)))

;;listes
(define pair?      			(lambda (x)   ($pair? x)))
(define car         		(lambda (x)   ($car x)))
(define cdr         		(lambda (x)   ($cdr x)))
(define cons        		(lambda (x y) ($cons x y)))
(define null?       		(lambda (x)   ($null? x)))
(define member      		(lambda (x y) ($member x y)))
(define assoc       		(lambda (x y) ($assoc x y)))
(define append      		(lambda (x y) ($append x y)))
(define reverse     		(lambda (x)   ($reverse x)))
(define length      		(lambda (x)   ($length x)))
(define map         		(lambda (x y) ($map x y)))    

;;char
(define char?       		(lambda (x)   ($char? x)))
(define char=?      		(lambda (x y) ($char=? x y)))
(define char<?      		(lambda (x y) ($char<? x y)))
(define char<=?     		(lambda (x y) ($char<=? x y)))
(define char>?      		(lambda (x y) ($char>? x y)))
(define char>=?     		(lambda (x y) ($char>=? x y)))

;;string
(define string?     		(lambda (x)   ($string? x)))
(define string=?    		(lambda (x y) ($string=? x y)))
(define string<?    		(lambda (x y) ($string<? x y)))
(define string<=?   		(lambda (x y) ($string<=? x y)))
(define string>?    		(lambda (x y) ($string>? x y)))
(define string>=?   		(lambda (x y) ($string>=? x y)))
(define substring   		(lambda (s x y) ($substring s x y)))
(define string-append       (lambda (x y)   ($string-append x y)))
(define make-string         (lambda (x y)   ($make-string x y)))
(define string-ref          (lambda (s x)   ($string-ref s x)))
(define string-set!         (lambda (s x y) ($string-set! s x y)))
(define string-length       (lambda (s)     ($string-length s)))

;;vector
(define vector?     		(lambda (x)     ($vector? x)))
(define make-vector         (lambda (x y)   ($make-vector x y)))
(define vector-ref          (lambda (v x)   ($vector-ref v x)))
(define vector-set!         (lambda (v x y) ($vector-set! v x y)))
(define vector-length       (lambda (v)     ($vector-length v)))

;;conversion
(define string->symbol      (lambda (x)   ($string->symbol x)))
(define symbol->string      (lambda (x)   ($symbol->string x)))
(define string->number      (lambda (x)   ($string->number x)))
(define number->string      (lambda (x)   ($number->string x)))
(define list->string        (lambda (x)   ($list->string x)))
(define string->list        (lambda (x)   ($string->list x)))
(define list->vector        (lambda (x)   ($list->vector x)))
(define integer->char       (lambda (x)   ($integer->char x)))
(define char->integer       (lambda (x)   ($char->integer x)))

;;Égalité
(define eq?         		(lambda (x y) ($eq? x y)))
(define eqv?        		(lambda (x y) ($eqv? x y)))
(define equal?      		(lambda (x y) ($equal? x y)))

(define number?      		(lambda (x) ($number? x)))
(define procedure?      		(lambda (x) ($procedure? x)))
(define symbol?      		(lambda (x) ($symbol? x)))

;;I/O
(define println     		(lambda (x)   ($println x)))
(define peek-char   		(lambda ()    ($peek-char)))
(define read-char   		(lambda ()    ($read-char)))
(define read-u8     		(lambda ()    ($read-u8)))
(define read        		(lambda ()    ($read)))
(define write-char  		(lambda (x)   ($write-char x)))
(define newline     		(lambda ()    ($newline)))
(define write       		(lambda (x)   ($write x)))
(define display     		(lambda (x)   ($display x)))	

;;contôle
(define force       		(lambda (x)   ($force x)))

;;manipulation de la mémoire
(define set-car!    		(lambda (x y) ($set-car! x y)))
(define set-cdr!    		(lambda (x y) ($set-cdr! x y)))

;;génération d'identifiants uniques
(define gensym              (lambda ()    ($gensym)))

;;échec de la macro match
(define match-failed        (lambda (x)   (println "MATCH FAILED") ($halt)))

;;;============================================================================
