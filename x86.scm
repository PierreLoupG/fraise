;;; File: x86.scm

;;;============================================================================

;; IR to x86 translation.

(define glo '())
(define fs '())
(define msize (* 42 1024 1024))
(define mahboiGC (string-append "mahboiGC" (symbol->string (gensym))))

(define namemangleraux
    (lambda (x)
        (let mangleloop ((lst x) (total 0) (i 0))
            (if (null? lst)
                (append (string->list (number->string total)) (append x (string->list (number->string i))))
                (cond
                    ((char=? #\? (car lst))
                        (begin
                            (set-car! lst #\A)
                            (mangleloop (cdr lst) (+ 1 total) (+ 1 i))))
                    ((char=? #\! (car lst))
                        (begin
                            (set-car! lst #\B)
                            (mangleloop (cdr lst) (+ 2 total) (+ 1 i))))
                    ((char=? #\: (car lst))
                        (begin
                            (set-car! lst #\C)
                            (mangleloop (cdr lst) (+ 3 total) (+ 1 i))))
                    ((char=? #\> (car lst))
                        (begin
                            (set-car! lst #\D)
                            (mangleloop (cdr lst) (+ 4 total) (+ 1 i))))
                    ((char=? #\< (car lst))
                        (begin
                            (set-car! lst #\E)
                            (mangleloop (cdr lst) (+ 5 total) (+ 1 i))))
                    ((char=? #\* (car lst))
                        (begin
                            (set-car! lst #\F)
                            (mangleloop (cdr lst) (+ 6 total) (+ 1 i))))
                    ((char=? #\$ (car lst))
                        (begin
                            (set-car! lst #\G)
                            (mangleloop (cdr lst) (+ 7 total) (+ 1 i))))
                    ((char=? #\- (car lst))
                        (begin
                            (set-car! lst #\H)
                            (mangleloop (cdr lst) (+ 8 total) (+ 1 i))))
                    ((char=? #\+ (car lst))
                        (begin
                            (set-car! lst #\I)
                            (mangleloop (cdr lst) (+ 9 total) (+ 1 i))))
                    ((char=? #\= (car lst))
                        (begin
                            (set-car! lst #\J)
                            (mangleloop (cdr lst) (+ 10 total) (+ 1 i))))
                    ((char=? #\& (car lst))
                        (begin
                            (set-car! lst #\K)
                            (mangleloop (cdr lst) (+ 11 total) (+ 1 i))))
                    ((char=? #\# (car lst))
                        (begin
                            (set-car! lst #\L)
                            (mangleloop (cdr lst) (+ 12 total) (+ 1 i))))
                    ((char=? #\@ (car lst))
                        (begin
                            (set-car! lst #\M)
                            (mangleloop (cdr lst) (+ 13 total) (+ 1 i))))
                    (else
                        (mangleloop (cdr lst) total (+ 1 i)))
                    )))))

(define namemangler
    (lambda (x) 
        (if (symbol? x)
            (string->symbol (list->string (namemangleraux (string->list (symbol->string x)))))
            (list->string (namemangleraux (string->list x))))))

(define (define-glo name)
	(let ((n (namemangler name)))
	    (if (member n glo)
	    	'()
	    	(set! glo (cons n glo)))))
		
(define (open-fs lab nparams)
	(set! fs (cons (cons lab nparams) fs)))

(define (close-fs)
	(begin*
		(cdr (list-ref fs 0))
		(set! fs (list-tail fs 1))))

(define (peephole ir) ;;optimization pour nettoyer ret/ move consécutifs
    (if (null? (cdr ir)) ;si ir ne contient qu'une seule instruction
        ir
        (let ((head1 (caar ir)) (head2 (caadr ir)))
            (if (and (eq? 'ret head1) (eq? 'ret head2))
                (peephole (cons (car ir) (cddr ir)))
                (if (and (eq? 'move head1) (eq? 'move head2)) ;si on a move x y et move z w
                    (let ((b2 (cadr (cadr ir))))
                        (if (<= b2 (cadr (car ir))) ;si z <= x, on peut combiner en move z y+w
                            (peephole (cons `(move ,b2 ,(+ (caddr (car ir)) (caddr (cadr ir)))) (cddr ir)))
                            (cons (car ir) (peephole (cdr ir)))))
                    (cons (car ir) (peephole (cdr ir))))))))
	
(define (ir->asm ir)
  (append (asm-prelude)
  				(asm-heap)
          (concatenate (map instr->asm* (peephole ir)))
          (asm-postlude)))

(define (asm-prelude)
  (list " .text\n"
        " .globl _main\n"
        " .globl main\n"
        "_main:\n"
        "main:\n"))
        
(define (asm-heap)
	(list " mov $" msize ", %rax\n" ;;création du from space
				" push %rax\n"
				" call mmap\n"
				" mov %rax, %r10\n"
				" mov %r10, _from_space(%rip)\n"
				" mov $" msize ", %rax\n" ;;création du to space
				" push %rax\n"
				" call mmap\n"
				" mov %rax, %r11\n"
				" mov %r11, _to_space(%rip)\n"
				" mov %rsp, _stack_base(%rip)\n"
				" lea global_base(%rip), %rax\n"
				" mov %rax, _glo_ptr(%rip)\n"
				" mov $0, %rax\n"
				" mov %rax, _glo_base(%rip)\n")) 

(define (asm-postlude)
  (append 
  	(list (asm-gc)
  	    "\n .data\n"
  	    " .align 8\n"
  	    " global_base: .quad 0\n")
  	(asm-glo glo)))

(define (asm-glo glo)
	(if (null? glo)
		'()
		(cons (list " .align 8\n" "glo_" (car glo) ": .quad 0\n") (asm-glo (cdr glo)))))

(define (asm-tag tag)
	(let ((l (gensym)))
  	(list " pop  %rax\n"
  				" and  $7, %rax\n" ; récupère le tag
  				" cmp  $" tag ", %rax\n" ; compare avec le paramètre
  				" je   " l "\n"
  				" push $1\n"
  				" jmp  end" l "\n"
  				" " l ":\n"
  				" push $9\n"
  				" end" l ":\n")))
        
(define (asm-test op)
	(let ((l (gensym)))
		(list " pop  %rbx\n"
		 			" pop  %rax\n"
		 			" cmp  %rbx, %rax\n"
		 			" " op " " l "\n"
					" push  $1\n"
					" jmp end" l "\n"
					" " l ":\n"
					" push  $9\n"
					" end" l ":\n")))
					 				
(define (asm-div)
	(list " pop  %rbx\n"                
				" mov  0(%rsp), %rax\n"
				" mov  $0, %rdx\n"
				" cqo\n"
				" idiv %rbx\n"))

(define (asm-gc)   ;;vérifie si l'allocation mémoire est possible, appelle GC sinon
    (let ((endgc (cons "endgc" (gensym))))
        (list ".align 8\n"
            " " mahboiGC ":\n"
            " pop %rax\n"         ;;taille -1
            " add $8, %rax\n"     ;;taille de la structure
            " mov %r10, %rdx\n"
            " add %rdx, %rax\n"
            " mov _from_space(%rip), %rcx\n"
            " sub %rcx, %rax\n"       ;;taille de la mémoire allouée en byte
            " mov $" msize ", %rbx\n"
            " cmp %rax, %rbx\n"
            " jg  " endgc "\n"
            " mov %r10, _alloc_ptr(%rip)\n" ;;mise à jour des variables du gc
            " mov %rsp, _stack_ptr(%rip)\n"
            " mov %rsp, %rax\n"
            " and $-16, %rsp\n"             ;;alignement de la pile
            " sub $8, %rsp\n"
            " push %rax\n"
            " call _gc\n"
            " pop %rsp\n"
            " mov _alloc_ptr(%rip), %r10\n"
            " " (gensym) ":\n"
            " .align 8\n"
            " " endgc ":\n"
            " pop %rdi\n"
            " jmp *%rdi\n")))
				
(define (instr->asm* instr)
  (append (list "##################### " (object->string instr) "\n")
          (instr->asm instr)))

(define (instr->asm instr)
  (match instr
  	
  	;;;============================================================================
  	;; Arithmétique  	
  	
  	;; addition
    ((add)
     	(list " pop  %rax\n"
           	" add  %rax, (%rsp)\n"))
    
   	;; soustraction
    ((sub)
     	(list " pop  %rax\n"
     				" sub  %rax, (%rsp)\n"))
    
    ;; multiplication
    ((mul)
    	(list " pop  %rax\n"
    	      " sar  $3, %rax\n"       ;;/8 pour la multiplication puis *8 pour avoir le tag 000
    				" mov  0(%rsp), %rbx\n"
    				" sar  $3, %rbx\n"
    				" imul %rax, %rbx\n"
    				" sal  $3, %rbx\n"
    				" mov  %rbx, 0(%rsp)\n"))
    							
    ;; quotient
    ((quotient)
    	(list (asm-div)
    	      " sal  $3, %rax\n"
    				" mov  %rax, 0(%rsp)\n"))	
    				
    ;; remainder
    ((remainder)
    	(list (asm-div)
    				" mov  %rdx, 0(%rsp)\n"))
    				
    ;; modulo
    ((modulo)
      (let ((ezmod (cons "ezmod" (gensym))) (endmod (cons "endmod" (gensym))))
		  	(list (asm-div)
		          " push %rax\n"      ;;copie du résultat de la division
		          " cmp  $0, %rax\n"
		          " jge   " ezmod "\n"   ;;si les 2 opérandes sont de même signe, modulo = remainder
		          " pop  %rax\n"       ;;sinon, le *vrai* modulo
		  				" sar  $3, %rbx\n"
		  				" push %rbx\n"      ;;copie du diviseur
		  				" imul %rax, %rbx\n"
		  				" sub  0(%rsp), %rbx\n";;permet d'obtenir le multiple du diviseur le plus proche du dividende  
		  				" sal  $3, %rbx\n"
		  				" pop  %rax\n"   
		  				" sub  %rbx, 0(%rsp)\n"
		          " jmp " endmod "\n"
		          " " ezmod ":\n"
		          " pop %rax\n"
		  				" mov  %rdx, 0(%rsp)\n"
		  				" " endmod ":\n")))
    ;;;============================================================================
	;; Logique
  	
  	;; egalité
   	((equal)
   		(asm-test "je"))
   					
   	;; infériorité
   	((inf)
	   	(asm-test "jl"))		   
		
 		((jit ,label)
 			(list " mov  0(%rsp), %rax\n"
 			      " cmp  $9, %rax\n"
 						" je   " label "\n"))
 		
 		((jif ,label)
 			(list " mov  0(%rsp), %rax\n"
 			      " cmp  $1, %rax\n"
 						" je   " label "\n"))
 						
 		((jiz ,label)
 			(list " mov  0(%rsp), %rax\n"
 			      " cmp  $0, %rax\n" 
 						" je   " label "\n"))
 		
 		((jl ,label)
 			(list " jmp  " label "\n"))
 			
 		((label ,label)
 			(list label ":\n"))
 			
	;;;============================================================================
	;; Création de littéraux et de structures de données
  	
	;; littéral à empiler
    ((push_lit ,val)
     	(list " mov $" val ", %rax\n"
     	    " push %rax\n"))

  	;; string
  	((make_str) 
   		(let ((loop (gensym)) (endGCcheck (gensym)))
    	(list	" lea " endGCcheck "(%rip), %rax\n" 
    	            " push %rax\n"
    	            " mov 16(%rsp), %rax\n" ;taille de la string
    	            " push %rax\n"
    	            " jmp " mahboiGC "\n" ;saut vers la vérification heap/GC
    	            " .align 8\n"
    	            " " endGCcheck ":\n"
    	            " pop  %rax\n" ; contenu
    				" mov  (%rsp), %rbx\n" ; taille
    				" pop  (%r10)\n" ; dépile et écrit la taille dans le tas
    				" push %r10\n" ; place le pointeur vers le vecteur sur la pile
    				" add  $5, (%rsp)\n" ; 4 = tag des vecteurs
    				" mov  $0, %rcx\n" ; variable i de la boucle
    				" " loop ":\n"
    				" cmp  %rbx, %rcx\n"
    				" jge  end" loop "\n" ; sort si taille atteinte
    				" push %rax\n" ; push le contenu à écrire
    				" add  $8, %r10\n" ; place le pointeur de tas sur la bonne case
    				" pop  (%r10)\n" ; écrit sur le tas
    				" add  $8, %rcx\n" ; i++
    				" jmp  " loop "\n"
    				" end" loop ":\n"
    				" add  $8, %r10\n" ; déplace le pointeur vers la prochaine case libre
    				)))
    
    ((str_ref)
    	(list " pop  %rax\n" ; position
    				" pop  %rbx\n" ; vecteur (adresse mémoire)
    				" add  $3, %rax\n"
    				" add  %rax, %rbx\n"
    				" mov  (%rbx), %rax\n"
    				" push %rax\n"
    				))				
    					
    ((str_lgt)
    	(list " pop  %rbx\n" ; vecteur (adresse mémoire)
    				" mov  -5(%rbx), %rax\n"
    				" push %rax\n"))
    
    ((str_set)
    	(list " pop  %rcx\n" ; valeur à écrire
    				" pop  %rax\n" ; position
    				" pop  %rbx\n" ; vecteur (adresse mémoire)
    				" add  $3, %rax\n"
    				" add  %rax, %rbx\n"
    				" mov  %rcx, (%rbx)\n"
    				" push $9\n"))
   	   	
   	;; vecteurs
   	((make_vec) 
   		(let ((loop (gensym)) (endGCcheck (gensym)))
    	    (list   " lea " endGCcheck "(%rip), %rax\n"
    	            " push %rax\n"
    	            " mov 16(%rsp), %rax\n"
    	            " push %rax\n"
    	            " jmp " mahboiGC "\n"
    	            " .align 8\n"
    	            " " endGCcheck ":\n"
    	            " pop  %rax\n" ; contenu
    				" mov  (%rsp), %rbx\n" ; taille
    				" pop  (%r10)\n" ; dépile et écrit la taille dans le tas
    				" push %r10\n" ; place le pointeur vers le vecteur sur la pile
    				" add  $4, (%rsp)\n" ; 4 = tag des vecteurs
    				" mov  $0, %rcx\n" ; variable i de la boucle
    				" " loop ":\n"
    				" cmp  %rbx, %rcx\n"
    				" jge  end" loop "\n" ; sort si taille atteinte
    				" push %rax\n" ; push le contenu à écrire
    				" add  $8, %r10\n" ; place le pointeur de tas sur la bonne case
    				" pop  (%r10)\n" ; écrit sur le tas
    				" add  $8, %rcx\n" ; i++
    				" jmp  " loop "\n"
    				" end" loop ":\n"
    				" add  $8, %r10\n" ; déplace le pointeur vers la prochaine case libre
    				)))
    
    ((vec_ref)
    	(list " pop  %rax\n" ; position
    				" pop  %rbx\n" ; vecteur (adresse mémoire)
    				" add  $4, %rax\n"
    				" add  %rax, %rbx\n"
    				" mov  (%rbx), %rax\n"
    				" push %rax\n"
    				))				
    					
    ((vec_lgt)
    	(list " pop  %rbx\n" ; vecteur (adresse mémoire)
    				" mov  -4(%rbx), %rax\n"
    				" push %rax\n"))
    
    ((vec_set)
    	(list " pop  %rcx\n" ; valeur à écrire
    				" pop  %rax\n" ; position
    				" pop  %rbx\n" ; vecteur (adresse mémoire)
    				" add  $4, %rax\n"
    				" add  %rax, %rbx\n"
    				" mov  %rcx, (%rbx)\n"
    				" push $9\n"))
    			
	;; paires/listes	
    ((make_cons)
    	(let ((endGCcheck (gensym)))
    	    (list   " lea " endGCcheck "(%rip), %rax\n"
    	            " push %rax\n"
    	            " push $8\n"        ;taille d'une paire -1
    	            " jmp " mahboiGC "\n"
    	            " .align 8\n"
    	            " " endGCcheck ":\n"
    	            " pop  %rax\n" ; cdr
    				" pop  %rbx\n" ; car
    				" mov  %rbx, 0(%r10)\n"
    				" mov  %rax, 8(%r10)\n"
    				" lea  3(%r10), %rax\n"
    				" add  $16, %r10\n"
    				" push %rax\n")))
    
    ((cons_car)
    	(list " pop  %rbx\n"
    				" mov  -3(%rbx), %rax\n"
    				" push %rax\n"))
    	
    ((cons_cdr)
    	(list " pop %rbx\n"
    				" mov 5(%rbx), %rax\n"
    				" push %rax\n"))
    
    ((set_car)
    	(list " pop  %rax\n" ; valeur
    				" pop  %rbx\n" ; paire
    				" mov  %rax, -3(%rbx)\n"
    				" push $9\n"))
    	
    ((set_cdr)
    	(list " pop  %rax\n" ; valeur
    				" pop  %rbx\n" ; paire
    				" mov  %rax, 5(%rbx)\n"
    				" push $9\n"))
   	
   	;;;============================================================================
	;; Gestion du typage
  	
    ((type)
    	(list " push  0(%rsp)\n"
				" push  $8\n"
				(asm-div)
				" mov  %rdx, 0(%rsp)\n"))
               
    ((is_num)
    	(asm-tag 0))
    
    ((is_char)
    	(asm-tag 2))
    
    ((is_pair)
    	(asm-tag 3))
    
    ((is_vec)
    	(asm-tag 4))
    
    ((is_str)
    	(asm-tag 5))
    
    ((is_sym)
    	(asm-tag 6))
    
    ((is_proc)
    	(asm-tag 7))
    	
    ((is_eq)
    	(let ((l (gensym)))
		  	(list " pop  %rax\n"
		  				" pop  %rbx\n"
		  				" cmp  %rax, %rbx\n"
		  				" je   " l "\n"
		  				" push $1\n"
		  				" jmp  end" l "\n"
		  				" " l ":\n"
		  				" push $9\n"
		  				" end" l ":\n")))
    
    ((i2c)
    	(list "add $2, (%rsp)\n"))
    	
    ((c2i)
    	(list "sub $2, (%rsp)\n"))
    
    ((sym2str)
    	(list "sub $1, (%rsp)\n"))
    	
		((str2sym)
    	(list "add $1, (%rsp)\n"))
   					 
   	;;;============================================================================
	;; Gestion des variables
   	  	
   	;; variables locales
   	((set ,pos)
   		(list " pop  %rax\n" ; valeur
   					" mov  %rax, " (* pos 8) "(%rsp)\n"
   					" push %rax\n"))				 
    
    ((push_loc ,pos)
			(list " push " (* pos 8) "(%rsp)\n"))
    
    ;; variables libres
   	((push_free ,i)
   		(list " pop  %rdi\n"
   					" push " (+ (* 8 (+ i 1)) 1) "(%rdi)\n"))
   	
   	((pop_free ,i)
   		(list " pop  %rdi\n"
   					" pop  " (+ (* 8 (+ i 1)) 1) "(%rdi)\n"))

    ;; variables globales
  	((push_glo ,name)
     	(list " push glo_" (namemangler name) "\n"))

   	((pop_glo ,name)
   		(let ((fullname (namemangler name)) (empty (null? glo)))
       		(begin 
       			(define-glo name)
       			(list " pop glo_" fullname "\n"
   			        (if empty
   			                (list " lea glo_" fullname "(%rip), %rcx\n" 
   			                    " mov %rcx, _glo_base(%rip)\n")  
   			                (list "")) 
				    " push $9\n")))) ; chaque instruction (ici define) doit empiler quelque chose

   	;;;============================================================================
	;; Gestion des procédures
  	
  		   
   	((push_proc ,lab)
   		(list " lea  " lab "(%rip), %rax\n"
   					" push %rax\n"))
    
	((push_ra ,lab)     ;identique à push_proc, serait utile pour distinguer les appels terminaux
	    (list " lea " lab "(%rip), %rax\n"
	        " push %rax\n"))   	
   	
   	;; fermetures   					 
   	((close ,nfree)
   		(let ((pop '()) (endGCcheck (gensym)))
	   		(for i 0 (- nfree 1) (set! pop (append `(" pop " ,(* 8 (+ i 2)) "(%r10)\n") pop)))
   			(append
   						;; cette ligne c'est pour la longueur de la fermeture dans la première case
   						(list " lea " endGCcheck "(%rip), %rax\n"
            	            " push %rax\n"
            	            " mov $" (* 8 (+ nfree 1)) ", %rax\n" ;taille de la fermeture -1
            	            " push %rax\n"
            	            " jmp " mahboiGC "\n"
            	            " .align 8\n"
            	            " " endGCcheck ":\n"
   						    " movq $" (* 8 (+ nfree 1)) ", (%r10)\n")
							pop
							(list " pop  8(%r10)\n" ; push la procédure
										" push %r10\n" ; place la fermeture sur la pile
										" add  $7, (%rsp)\n" ; 7 = tag des procédures
										" add  $" (* (+ nfree 2) 8) ", %r10\n")))) ; déplace le pointeur de tas
   	
   	((proc ,lab ,nparams)
   		(begin 
   			(open-fs lab nparams)
		 		(list " .align 8\n"
		 					" " lab ":\n"
		 					" cmp  $" nparams ", %rax\n"
		 					;" jnz nargs_error\n"
		 					)))
	
	((jump ,nargs)
	    (list " mov $" nargs ", %rax\n"
	        " mov 8(%rsp), %rdi\n"
	        " jmp *1(%rdi)\n"))  ;;saut vers le code de la fermeture stockée à l'addresse dans %rdi
	        
    ((retaddr ,lab)     ;;empile une addresse de retour
        (list " .align 8\n"
            " " lab ":\n"))

    ((ret ,i ,n)
   		(list "bp" (gensym) ":\n" 
   		    
   		" mov  " (* i 8) "(%rsp), %rdi\n"
   					" mov  (%rsp), %rax\n"
   					" add  $" (* 8 (+ n 1)) ", %rsp\n"
   					" push %rax\n"
   					" jmp  *%rdi\n"))    
    
  	;;;============================================================================
	;; I/O
  	
  	;; écriture d'un octet
  	((write_u8)
   		(list " pop %rax\n"
	       		" sar $3, %rax\n"
	       		" push %rax\n"
	        	" call putchar\n"
	        	" push $1\n")) 
		       	      
    ;; lecture d'un octet
    ((read_u8)
    	(list " call getchar\n"
    	    	" sal  $3, %rax\n"
            " push %rax\n"))

  ;;;============================================================================
	;; Gestion de pile
  	
  ((move ,k ,n)                   ;;instruction générique de manipulation de pile 
  	    (append
  	        (let moveloop ((i (- k 1)) (n2 n))  ;;dépile k valeurs, dépile et ignore n valeurs,
  	                                            ;;empile le bloc de k valeurs
  	                (if (>= i 0)
  	                    (append
  	                        (list " mov " (* i 8) "(%rsp), %rax\n"
                                " mov %rax, " (* 8 (+ i n2)) "(%rsp)\n")
                            (moveloop (- i 1) n2))
                        (list "")))
            (list " add $" (* 8 n) ", %rsp\n")))                             
           
	((exit)
  		(list " mov  $0, %rax\n"
        		" ret\n\n\n"))
    
    ;;;============================================================================
	;; veilles fonctions, présentes purement à des fins de nostalgie
    
    ;;contrôle
   	((ret ,i)
   		(list " mov  " (* i 8) "(%rsp), %rdi\n"
   					" mov  (%rsp), %rax\n"
   					" add  $" (* 8 (+ (+ (close-fs) 1) i)) ", %rsp\n" ; fs + 1 (adresse retour) + i
   					" push %rax\n"
   					" jmp  *%rdi\n"))
    
    ;; appel de procédure
   	((call ,nargs)
   		(let ((retlab (cons "return" (gensym))))
		 		(list	" mov  (%rsp), %rdi\n" 
		 					" lea  " retlab "(%rip), %rax\n"
		 					" push %rax\n"
		 					" mov  $" nargs ", %rax\n"
		 					" jmp  *1(%rdi)\n"
		 					" .align 8\n"
		 					" " retlab ":\n")))
		 					
    ;;pile
    ((dealloc ,n)
		(if (<= n 0)
			(list "")
			(list " mov  0(%rsp), %rax\n"
					" add  $" (* (+ n 1) 8) ", %rsp\n"
					" push %rax\n")))
    
    ((drop)
    	(list " add  $8, %rsp\n"))
    	       
    ;; affichage
    ((println)
   		(list " call print_ln\n"))
   		
	((load_str ,label)
    	(list " lea  " label "(%rip), %rax\n"
    				" push %rax\n"))  
	 		
    (_
     (error "instr->asm encountered the unknown instruction" instr))))

;;;============================================================================


