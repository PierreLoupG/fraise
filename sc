#! /usr/bin/env gsi

(include "read.scm")
(include "match.scm")
(include "utils.scm")
(include "genconst.scm")
(include "x86.scm")
(include "codegen.scm")
(include "parser.scm")
(include "compiler.scm")
