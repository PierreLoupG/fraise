;;; File: compiler.scm

;;;============================================================================

;; The run function executes a process and captures its exit
;; status and output.  This is used to execute the gcc compiler.

(define (run path . args)
  (let* ((port
          (open-process (list path: path
                              arguments: args)))
         (output
          (read-line port #f))
         (status
          (process-status port)))
    (close-port port)
    (cons status output)))

(define gcc-link-option
  (if (equal? (cadr (system-type)) 'apple)
      "-Wl,-no_pie"
      "-Wall"))

;; (compile filename) is the main function of the compiler.  The
;; parameter is the path of the source code file to compile.
;; If the compilation succeeds three files are created:
;;
;;   filename.ir   the intermediate code generated by the compiler
;;   filename.s    the assembler code generated by the compiler
;;   filename.exe  the executable program generated by the compiler

(define (compile filename)
 
  ;((ast (append (parse-program "lib.scm") (parse-program filename)))
 
  (let* ((ast (append (parse-program "lib.scm") (parse-program filename))) ;; parse program
         (ir (compile-program ast))     ;; compile AST to IR
         (code (ir->asm ir)))           ;; translate IR to assembler code

    (let* ((base-filename (path-strip-extension filename))
           (ir-filename (string-append base-filename ".ir"))
           (asm-filename (string-append base-filename ".s"))
           (exe-filename (string-append base-filename ".exe"))
           (root-dir (path-directory (this-source-file))))

      ;; write generated IR

      (with-output-to-file ir-filename (lambda () (pretty-print ir)))

      ;; write generated assembler code

      (with-output-to-file asm-filename (lambda () (print code)))

      ;; assemble generated assembler code and link with rts.o

      (let ((x (run "gcc"
                    "-m64"
                    "-no-pie"
                    gcc-link-option
                    "-o"
                    exe-filename
                    (path-expand "rts.o" root-dir)
                    (path-expand "mmap.o" root-dir)
                    (path-expand "gc.o" root-dir)
                    asm-filename)))

        ;; return exit status

        (car x)))))

(define (main . filenames)
  (for-each compile filenames))

;;;============================================================================
