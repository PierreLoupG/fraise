#! /usr/bin/env gsi

;;;============================================================================

;;; File: "run-unit-tests.scm"

;;;----------------------------------------------------------------------------

(define cleanup? #f)

(define nb-good 0)
(define nb-fail 0)
(define nb-total 0)
(define start 0)

(define (num->string num w d) ; w = total width, d = decimals
  (let ((n (floor (inexact->exact (round (* (abs num) (expt 10 d)))))))
    (let ((i (quotient n (expt 10 d)))
          (f (modulo n (expt 10 d))))
      (let ((si (string-append
                  (if (< num 0) "-" "")
                  (if (and (= i 0) (> d 0)) "" (number->string i 10))))
            (sf (number->string (+ f (expt 10 d)) 10)))
        (if (> d 0)
          (string-set! sf 0 #\.)
          (set! sf ""))
        (let ((lsi (string-length si))
              (lsf (string-length sf)))
          (let ((blanks (- w (+ lsi lsf))))
            (string-append (make-string (max blanks 0) #\space) si sf)))))))

(define (show-bar nb-good nb-fail nb-total elapsed)

  (define (ratio n)
    (quotient (* n (+ nb-good nb-fail)) nb-total))

  (if (tty? (current-output-port))

      (let* ((bar-width 20)
             (bar-length (ratio bar-width)))

        (define (esc x)
          x)

        (print "\r"
               "["
               (esc "\33[32;1m") (num->string nb-good 2 0) (esc "\33[0m")
               "|"
               (esc "\33[31;1m") (num->string nb-fail 2 0) (esc "\33[0m")
               "] "
               (num->string (ratio 100) 3 0)
               "% "
               (make-string bar-length #\#)
               (make-string (- bar-width bar-length) #\.)
               (num->string elapsed 5 1)
               "s"
               (esc "\33[K")))))

(define (run input path . args)
  (let ((port
         (open-process (list path: path
                             arguments: args
                             ;;stdout-redirection: #f
                             stderr-redirection: #t
                             ))))
    (write-string input port)
    (close-output-port port)
    (let* ((output
            (read-line port #f))
           (status
            (process-status port)))
      (close-port port)
      (cons status (if (eof-object? output) "" output)))))

(define (test-using-mode file mode)
  (let ((input
         (or (get-input (string-append (path-strip-extension file)
                                       ".input"))
             "")))

    (cond ((equal? mode "gsi")
           (run input
                "gsi" "-:d-,fu" "-e" "(define (read-u8) (let ((x (##read-u8 ##stdin-port))) (if (eof-object? x) -1 x)))" file))

          ((equal? mode "sc")
           (let ((x (run ""
                         "./sc" file))
                 (exe (string-append (path-strip-extension file)
                                     ".exe")))
             (if (= (quotient (car x) 256) 0)
                 (if (file-exists? exe)
                     (run input
                          exe)
                     (cons 256 (cdr x)))
                 x))))))

(define (get-input filename)
  (if (file-exists? filename)
      (call-with-input-file
          filename
        (lambda (port)
          (let ((x (read-line port #f)))
            (if (eof-object? x)
                ""
                x))))
      #f))

(define (get-expected-output filename)
  (call-with-input-file
      filename
    (lambda (port)
      (let ((lines (read-all port (lambda (p) (read-line p #\newline #t)))))
        (let loop ((rev-lines (reverse lines))
                   (output '()))
          (if (and (pair? rev-lines)
                   (>= (string-length (car rev-lines)) 1)
                   (char=? (string-ref (car rev-lines) 0) #\;))
              (loop (cdr rev-lines)
                    (cons (substring (car rev-lines)
                                     1
                                     (string-length (car rev-lines)))
                          output))
              (apply string-append output)))))))

(define (ends-with? str suffix)
  (let ((len-str (string-length str))
        (len-suffix (string-length suffix)))
    (and (>= len-str len-suffix)
         (equal? (substring str (- len-str len-suffix) len-str)
                 suffix))))

(define (trim-filename file)
  (if (and (>= (string-length file) (string-length default-dir))
           (string=? (substring file 0 (string-length default-dir))
                     default-dir))
      (substring file (string-length default-dir) (string-length file))
      file))

(define (test file)
  (for-each

   (lambda (mode)

     (print " " (trim-filename file))
     (force-output)

     (let* ((result (test-using-mode file mode))
            (status (car result))
            (output (cdr result))
            (expected-output (get-expected-output file))
            (ok? (if (ends-with? (path-strip-extension file) "_crash")
                     (not (= status 0))
                     (and (= status 0)
                          (equal? output expected-output)))))
       (if ok?
           (set! nb-good (+ nb-good 1))
           (begin
             (set! nb-fail (+ nb-fail 1))
             (println)
             (println "*************************************** FAILED TEST")
             (if (= status 0)
                 (begin
                   (println "*** output:")
                   (println output)
                   (println "*** expected output:")
                   (println expected-output))
                 (begin
                   (println "*** status: " status)
                   (print output)))
             (println "***************************************")))

       (show-bar nb-good
                 nb-fail
                 nb-total
                 (- (time->seconds (current-time)) start))))

   modes))

(define (run-tests files)

  (set! nb-good 0)
  (set! nb-fail 0)
  (set! nb-total (length files))
  (set! start (time->seconds (current-time)))

  (for-each test files)

  (print "\n")

  (if (= nb-good nb-total)
      (begin
        (print "PASSED ALL " nb-total " UNIT TESTS\n")
        (exit 0))
      (begin
        (print "FAILED " nb-fail " UNIT TESTS OUT OF " nb-total " (" (num->string (* 100. (/ nb-fail nb-total)) 0 1) "%)\n")
        (exit 1))))

(define (find-files file-or-dir filter)
  (if (eq? (file-type file-or-dir) 'directory)

      (apply
       append
       (map
        (lambda (f)
          (find-files (path-expand f file-or-dir) filter))
        (directory-files file-or-dir)))

      (if (filter file-or-dir)
          (list file-or-dir)
          (list))))

(define (list-of-scm-files args)
  (apply
   append
   (map
    (lambda (f)
      (find-files f
                  (lambda (filename)
                    (equal? (path-extension filename) ".scm"))))
    args)))

(define modes '())

(define default-dir
  (let* ((cd (current-directory))
         (len (string-length cd)))
    (string-append "unit-tests" (substring cd (- len 1) len))))

(define (main . args)

  (let loop ()
    (if (and (pair? args)
             (> (string-length (car args)) 1)
             (char=? #\- (string-ref (car args) 0)))
        (begin
          (set! modes
                (cons (substring (car args) 1 (string-length (car args)))
                      modes))
          (set! args (cdr args))
          (loop))))

  (if (null? args)
      (set! args (list default-dir)))

  (if (null? modes)
      (set! modes '("sc")))

  (run-tests (list-of-scm-files args)))

;;;============================================================================
