;; Fichier "genconst.scm"

;; Ce fichier montre une façon de convertir une constante littérale
;; en une séquence de définitions de variables globales qui construisent
;; la constante à l'exécution.
;;
;; L'idée est de transformer un programme source tel que :
;;
;; (write '#(5 abc ("allo" abc #\x)))
;;
;; en
;;
;; (define _1
;;   (let ((str ($make-string 3 #\space)))
;;     ($string-set! str 0 #\a)
;;     ($string-set! str 1 #\b)
;;     ($string-set! str 2 #\c)
;;     str))
;; (define _2 ($string->symbol _1))
;; (define _3
;;   (let ((str ($make-string 4 #\space)))
;;     ($string-set! str 0 #\a)
;;     ($string-set! str 1 #\l)
;;     ($string-set! str 2 #\l)
;;     ($string-set! str 3 #\o)
;;     str))
;; (define _4 ($cons #\x '()))
;; (define _5 ($cons _2 _4))
;; (define _6 ($cons _3 _5))
;; (define _7
;;   (let ((vec ($make-vector 3 0)))
;;     ($vector-set! vec 0 5)
;;     ($vector-set! vec 1 _2)
;;     ($vector-set! vec 2 _6)
;;     vec))
;; (write _7)

(define const-code '())
(define const-count 0)
(define const-table '())

(define (def-const obj construct)
  (set! const-count (+ const-count 1))
  (let ((var (string->symbol (string-append "_" (number->string const-count)))))
    (set! const-code (cons `(define ,var ,construct) const-code))
    (set! const-table (cons (cons obj var) const-table))
    var))

(define (gen-const obj)
  (let ((x (assq obj const-table)))
    (if x
        (cdr x)
        (cond ((pair? obj)
               (def-const
                 obj
                 `($cons ,(gen-const (car obj))
                         ,(gen-const (cdr obj)))))

              ((vector? obj)
               (def-const
                 obj
                 `(let ((vec ($make-vector ,(vector-length obj) 0)))
                    ,@(map (lambda (i)
                             `($vector-set! vec ,i ,(gen-const (vector-ref obj i))))
                           (iota (vector-length obj)))
                    vec)))

              ((string? obj)
               (def-const
                 obj
                 `(let ((str ($make-string ,(string-length obj) #\space)))
                    ,@(map (lambda (i)
                             `($string-set! str ,i ,(string-ref obj i)))
                           (iota (string-length obj)))
                    str)))

              ((symbol? obj)
               (def-const
                 obj
                 `($string->symbol ,(gen-const (symbol->string obj)))))

              ((or (number? obj) (boolean? obj) (char? obj))
               obj)

              (else
               `(quote ,obj))))))

;(define const (gen-const '#(5 abc ("allo" abc #\x))))

;(for-each
; pretty-print
; (reverse const-code))

;(pretty-print `(write ,const))
