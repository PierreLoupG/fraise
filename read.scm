;;; File: read.scm

;;;============================================================================

;; S-expression parser.

;; This only handles a few syntactic forms.  It must be extended to
;; handle comments, booleans, characters, quotations, etc.

;; To have the compiler use this code to read source code, the parse-expr
;; function in parser.scm must be defined like this:
;;
;; (define (parse-expr)
;;   (read*))

(define (read*)
  (let ((c (peek-char-non-whitespace)))
    ;(display c)
    (cond 
    	  ((eof-object? c) c)
          ;; caractère "(" = lit et renvoie la liste
          ((char=? c #\()
           	(read-char) ;; skip "("
           	(read-list))
          ;; caractère "'" = lit et renvoie la liste sans l'évaluer
          ((char=? c #\')
           	(read-char)
           	(cons 'quote (cons (read*) '()))) 
          ;; caractère "#" = traitement des boolean ou chars
          ((char=? c #\#)
           	(read-char)
          	(read-hashtag))
          ;; caractère " = traitement d'un string
          ((char=? c #\u0022)
          	(read-char)
          	(list->string (read-string)))
          ;; caractère ";" = lit le commentaire sans le renvoyer
          ((char=? c #\u003b)
            ;(read-char) ;; traitement des commentaires
            (read-comment)
            (read*)) ;; recommence la lecture = saute le commentaire sans rien retourner
          (else
           	(read-char) ;; skip first char
           	(let ((s (list->string (cons c (read-symbol)))))
           		(or (string->number s)
              	(string->symbol s)))))))

(define (read-list)
  (let ((c (peek-char-non-whitespace)))
    ;(display c)
    (if (char=? c #\))
        (begin
          (read-char) ;; skip ")"
          '())
        ;; si la liste n'est pas finie on recommence la lecture 
        ;; pour d'éventuelle liste imbriquée
        (if (char=? c #\.)   
        (begin 
            (read-char)
            (let ((input (read-list)))
                (if (null? (cdr input))
                    (car input)
                    (error "ill-formed improper list: more than 1 element after dot: " input))))
        (let ((first (read*)))
            (let ((rest (read-list)))
                (cons first rest)))))))

(define (read-symbol)
  (let ((c (peek-char)))
    ;(display c)
    (if (or (eof-object? c)
            (char=? c #\()
            (char=? c #\))
            (char<=? c #\space))
        '()
        (begin
          (read-char)
          (cons c (read-symbol))))))
          
(define (read-string)
	(let ((c (peek-char)))
		;(display c)
		(if (or (eof-object? c)
						(char=? c #\u0022)
						(char=? c #\newline))
				(begin
					(read-char) ;; skip " (sinon le " fermant sera considéré comme un ouvrant)
					'())	
				(if (char=? c #\\) ;; si on lit un charactère échappé
			      (begin
				    (read-char)
				    (if (or (char=? (peek-char) #\u0022) (char=? (peek-char) #\\) 
				            (char=? (peek-char) #\|) (char=? (peek-char) #\?)) ;; ", \, | ou ? échappé
				      (cons (read-char) (read-string))
				      (if (char=? (peek-char) #\n) ;; un saut de ligne échappé
				        (begin 
				          (read-char)
				          (cons #\newline (read-string)))
			            (if (char=? (peek-char) #\a)
			              (begin 
				            (read-char)
				            (cons #\u0007 (read-string)))
				          (if (char=? (peek-char) #\b)
			                (begin 
				              (read-char)
				              (cons #\u0008 (read-string)))
				            (if (char=? (peek-char) #\t)
			                  (begin 
				                (read-char)
				                (cons #\u0009 (read-string)))
				              (if (char=? (peek-char) #\v)
			                    (begin 
				                  (read-char)
				                  (cons #\u000b (read-string)))
				                (if (char=? (peek-char) #\f)
			                      (begin 
				                    (read-char)
				                    (cons #\u000c (read-string)))
				                  (if (char=? (peek-char) #\r)
			                        (begin 
				                      (read-char)
				                      (cons #\u000d (read-string))))))))))))				    				    
				    (begin
					 		(read-char)
					 		(cons c (read-string)))))))
          
(define (read-hashtag)
  (let ((c (peek-char)))
    ;(display c)
    (cond
    		;; boolean true
    		((char=? c #\t) (begin (read-char) #t))
    		;; boolean false
    		((char=? c #\f) (begin (read-char) #f))
    		;; vector
    		((char=? c #\() (begin (read-char) (list->vector(read-list))))
    		;; caractère unique
    		((char=? c #\\)
    			(read-char) ;; skip "\"
    			(let ((r (list->string (cons (read-char) (read-symbol))))) ;; r = le caractère (ou la valeur unicode)
    				(cond

    				    ((eq? 1 (string-length r)) (string-ref r 0)) ;;1 caractère, ex: x -> #\x
    				    
    				    ((and (char=? #\u (string-ref r 0)) 
    				    			(> 6 (string-length r))) 
    				    			;;représentation hex du char, 4 digits
    				    			(integer->char (string->number (string-append "#x" (substring r 1 (string-length r)))))) 
    				    
    				    ((and (char=? #\U (string-ref r 0)) 
    				    			(> 10 (string-length r))) 
    				    			;;représentation hex du char, 8 digits
    				    			(integer->char (string->number (string-append "#x" (substring r 1 (string-length r)))))) 
    				    
    				    ((or (string=? "nul" r) (string=? "null" r)) #\null)
    				    
    				    ((string=? "backspace" r) #\backspace)
    				    
    				    ((string=? "tab" r) #\tab)
    				    
    				    ((or (string=? "newline" r) (string=? "linefeed" r)) #\newline)
    				    
    				    ((string=? "vtab" r) #\vtab)
    				    
    				    ((string=? "page" r) #\page)
    				    
    				    ((string=? "return" r) #\return)
    				    
    				    ((string=? "space" r) #\space)
    				)))
    		;; sinon liste vide
    		(else '()))))

(define (read-single-char)
	(let ((c (peek-char)))
		;(display c)
		(if (or (eof-object? c)
						(char=? c #\newline)
						(char<=? c #\space))
				'()
				(begin
					(read-char)
					(cons c (read-single-char))))))

(define (read-comment) ;;lit un commentaire jusqu'à un eof ou \n et retourne nil
  (let ((c (peek-char)))
    ;(display c)
    (if (or (eof-object? c)
            (char=? c #\newline))
        '()
        (begin
          (read-char)
          (read-comment)))))

(define (peek-char-non-whitespace)
  (let ((c (peek-char)))
    ;(display c)
    (if (or (eof-object? c)
            (char>? c #\space))
        c
        (begin
          (read-char)
          (peek-char-non-whitespace)))))

;; Test it...

(define (test-read input expected)
  (let ((x (with-input-from-string input read*)))
    (if (not (equal? x expected))
        (begin (display "*** failed reading: ") (display input) (display " (expected: ") (display expected) (display ") > ") (display x)(display "\n"))
        (println "*** sucess reading: " input " (expected: " expected ") > " x))))

;; TODO: add tests!
;;(test-read "(if (+ 2 2)(+ 1 1)(+ 1 1))" '(if (+ 2 2)(+ 1 1)(+ 1 1)))
;;(test-read "   ( )   "  '())
;;(test-read "(#\\A #\\B)" '(#\A #\B))
;;(test-read "(a #\\newline)" '(a #\newline))
;;(test-read "#(1 2 3 4 5)" '#(1 2 3 4 5))
;;(test-read "(a . b)" '(a . b))
;;(test-read "(   1 2 3)" '(1 2 3))
;;(test-read "(allo pomme)" '(allo pomme))
;;(test-read "(\"Hello\" \"world\")" '("Hello" "world"))
;;(test-read "(   1 2 . (3 . 4))" '(1 2 . (3 . 4)))
;;(test-read "((a) . b)" '((a) . b))
;;(test-read "#f"         #f)
;;(test-read "  \n\n #t"  #t)
;;(test-read "#\\x" #\x)
;;(test-read "#\\;" #\u003b)
;;(test-read "#\\u003b" #\u003b)
;;(test-read "  \"rouge\"  " "rouge")
;;(test-read "  \"il dit \\\"bonjour\\\"\"  " "il dit \"bonjour\"")
;;(test-read "'(a b c)" ''(a b c))
;;(test-read "''''''(a b c)" '''''''(a b c))
;;(test-read " ;comment\n ( 1 ;comment\n 2)"  '(1 2))
;;(test-read " (if ;comment\n (#t)(+ 1 ;comment\n 2) (#f))"  '(if (#t)(+ 1 2) (#f)))

;;;============================================================================
