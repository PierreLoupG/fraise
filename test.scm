;;; File: test.scm

;;; les tests importants permettant souvent sujet à de bug sont signalé par
;;; la balise '#!<name>'
;;;
;;; - la balise #!VAR est utilisé pour des tests sur les variables, les
;;; capture de nom etc
;;; - la balise #!CLOSE pour les tests de fermeture
;;;
;;; faites une recherche de balise pour tester rapidement une fonctionnalité




;(define compose (lambda (f g x) (f (g x))))

;(letrec ((loop (lambda (i)
; (println i)
;	(loop ($+ i 1)))))
;	(loop 0))
	
;(define x (lambda () (x)))
;(x)

;(println "cocolapin")


;(define f
;    (lambda (a b)
;        (lambda (a t)
;            (lambda (x y z)
;                a))))

;(define h (f 1 2))
;(define g (h 3 4))
;(println (g 10 20 30))

;($display-string-aux "cocolapin" 0)


;(define f
;  (lambda (x)
;    ($+ x x)))
    
;(define g
;  (lambda (a)
;    (f a)))
    
;(g 4)


;(define f
;  (lambda (x)
;    (if ($< 0 x)
;      (begin
;        ($write-u8 x)
;        #t)
;      #f)))
      
;(f 97)


;(define x (lambda (i) (println i) (x (+ 1 i))))
;(x 0)

;(define display-string-aux
;  (lambda (str i)
;    (if i
;       (display-string-aux str #f)
;        #f)))
;(display-string-aux "cocolapin" #t)

;(define display-string-aux
;  (lambda (str i)
;    (if ($< i ($string-length str))
;        (begin
;          ($write-u8 ($string-ref str i))
;          (display-string-aux str ($+ i 1)))
;        #f)))

;(display-string-aux "cocolapin" 0)

;(define f (lambda (x) ($+ 97 x)))
;($write-u8 (f 0 5))
;(define j (lambda (x k) (f x)))
;(define g (lambda (x) (if x ($write-u8 (j x 1337)) ($write-u8 28))))
;(g 0)
;(println "cocolapin")
;(println "kokopalin")

;(define f (lambda (x)
; (let ((a ($+ x 1)))
;   (let ((b ($- x 1)))
;     (let ((c ($* a b)))
;       c)))  ))

;(display (f 1))

;(define v (make-vector 4 4))
;(println v)

;(define ($string->symbolaux x) (println x))


;(if ($eq? #f #f) (println 1) (println 9))

;(println '#(0 foo #(cocolapin 2)))
;(println 'foo)
;(println 'bar)
;(println '())
;(println '(1 2 3))
;(println 'foo)
;(println '#(1 2 3))

;(define x (delay (println 42)))
;(define x (+ 1 3))
;(println "cocolapin")
;(println x)
;(printenv)
;(force x)

;;($gc)

;;---

;(define foo 
;	(lambda (n)
;		(let ((i n))
;			(println (+ i (* n 2))))))

;(foo 3)

;9

;;---

;(println "hey\nyeh")	 		

;(foo '(1 2 3) 777)
;(foo '(9 8 7) 333)

;(define (foo x n) (println x) (println n))

;(println (match '(1 2 3) ((1 2) 888) ((1 2 3) 999)))

;(println '1)

;(foo (+ 1 1) 1)

;(define (foo x y)
;	(if (< x 100) (println #t) (println #f)))

;(foo (+ 1 1) 5)

;(define (t) 4)

;(let ((c 1))
;	(println (+ 1 1))
;	(println c))

;; (t) 2
;; (+ i 1) 0
;; ($+ i 1) 1
;; (t 4) 1
;; (t 4 5) 0

;(letrec ((loop (lambda (i)
;	(println i)
;	(loop (+ i 1)))))
;	(loop 0))

;(let $display-vector-loop ((i 0) (n 4))
;		(println i)
;   (if ($< i n)
;        ($display-vector-loop (+ i 1) n)
;        (println #f)))

;;;============================================================================
;; and

;(println (and #t #t #f (println 4)))

;#f

;;;============================================================================
;; or

;(println (or #f ($not (println 4))(println 5)))

;4
;#t

;;;============================================================================
;; define

;(define v 3)
;(println v)
;(define (f x) (println x))
;(f 1)
;(f v)

;3
;1
;3 

;;---

;(define (loop n)
;	(if ($<= n 0)
;		(println n)
;		(begin
;			(println n)
;			(loop ($- n 1)))))
			
;(loop 4)

;4
;3
;2
;1
;0

;;;============================================================================
;; conversion 

;(println ($number? ($+ 1 1)))
;(println ($integer->char 97))
;(println ($char->integer #\a))

;;;============================================================================
;; lambda

;(define plus (lambda (a b) ($+ a b)))
;(println (plus 1 2))
;(define power (lambda (x) ($* x x)))
;(println (plus 1 (power 3)))

;3
;10

;;---

;((lambda (n) (println n)) 5)

;5

;;---

;;#!CLOSE

;(define f
;	(lambda (a b)
;		(lambda (a t)
;			(lambda (x y z)
;				($+ x a)))))

;(define h (f 1 2))
;(define g (h 3 4))
;(println (g 10 20 30))

;13
	
;;---

;;#!VAR
;;#!CLOSE

;(define f
;	(lambda (n)
;		(g (lambda (x)
;				($+ n x))
;			(lambda (y)
;				(println n)
;				(set! n y)
;				(println ($* n 2))))))
			
;(define g
;	(lambda (read-n write-n)
;		(write-n (read-n 1))
;		(read-n 0)))

;(println (f 42))

;42
;86
;43

;;---

;(define k
;	(lambda (x)
;		($+ x 1)))
		
;(println (k (k 2)))

;4

;;---

;;#!CLOSE

;(define f
;	(lambda (a b c)
;		(lambda (x y z)
;			($+ a y))))
			
;(define g (f 1 2 3))
;(println (g 20 10 0))

;11

;;---

;;#!VAR

;(define f
;	(lambda (n)
;		(lambda (x)
;				($set! x ($+ x n))
;				(println x))))
			
;(define g (f 5))
;(println (g 12))

;17

;;---

;;#!VAR

;(define f
;	(lambda (n)
;		(lambda (x)
;			(println n)
;			(println ($+ n n))
;			(let ((y 1))
;				(println #\Z)
;				(println ($+ y n)))
;			(println #\a)
;			($+ n x))))

;(define g (f 4))
;(println (g 10))

;4
;8
;Z
;5
;a
;14

;;---

;;#!VAR
;;#!CLOSE

;(define f
;	(lambda (n)
;		(g n)))
	
;(define g
;	(lambda (x)
;		(println ($* x x))))		

;(f 3)

;9

;;;============================================================================
;; string

;(define s ($make-string ($+ 3 3) #\a))
;(println ($string-length s))
;(println ($string-ref s 2))
;($string-set! s 2 #\b)
;(println ($string-ref s 2))
;(println "cocolapin")

;(println '#(1 2 3))
;(println '#(1 2 3))
;(println '#(1 2 (3 4 5)))
;(println '(#\a #\b))
;(println '(#\a #\b))



;;;============================================================================
;; vector

;(define v ($make-vector 6 2))
;(println (vector-length v))
;(println (vector-ref v 2))
;(vector-set! v 2 4)
;(println (vector-ref v 2))
;(println v)

;;;============================================================================
;; cons , car , cdr , set-car! , set-cdr!

;(define c ($cons 1 2))
;(println ($car c))
;(println ($cdr c))
;($set-car! c 3)
;($set-cdr! c 4)
;(println ($car c))
;(println ($cdr c))

;1
;2
;3
;4

;;---

;(define x ($cons 5 ($cons 6 7)))
;(println ($car x))
;(println ($car ($cdr x)))
;(println ($cdr ($cdr x)))
;($set-car! x 2)
;(println ($car x))

;5
;6
;7
;2

;;;============================================================================
;; eq?

;(println (eq? 1 1)) ;#t
;(println (eq? 1 2)) ;#f
;(define a 1) 
;(define b 1)
;(println (eq? a 1)) ;#t
;(println (eq? a b)) ;#t
;(define v (make-vector 2 1))
;(define x (make-vector 2 1))
;(println (eq? v x)) ;#f
;(define v x)
;(println (eq? v x)) ;#t

;;;============================================================================
;; set!

;(define nb 10)
;(println nb)
;($set! nb ($+ nb 1))
;(println nb)

;10
;11

;;---


;(define (func x)
;	($set! x 3)
;	(println x))

;(func 4)

;3

;;---

;((lambda (x)
;	($set! x ($* x x))
;	(println x)) 5)
	
;25

;;---

;(let ((a 3))
;	((lambda (x)
;		($set! a ($* x x))
;		(println x)
;		(println a)) 5))

;5
;25

;;---

;(let ((c 4))
;	($set! c 9)
;	(println c))

;9

;;;============================================================================
;; let

;(let () (println #\1) (let ((x 2)) (println x)))

;1
;2

;;---

;(let ((a 1))
;	(define (foo b) (+ b b))
;	(println (+ a (foo 4))))

;9
	
;;---	
	
;(let ()
;	(define x 4)
;	(define y 5)
;	(println ($+ y x)))

;;---

;(let ((n 3))
;	(begin
;		(if ($= 0 0) 4 5)
;		($+ 1 1)
;		($+ 2 2)
;		(println n)))

;3

;;---

;;#!VAR
;;#!CLOSE

;(define foo 
;	(lambda (callback)
;		(callback)
;		(callback)
;		(callback)))

;(let ((i 0))
;	(foo
;		(lambda ()
;			(set! i ($+ i 1))
;			(println i))))
			
;1
;2
;3


;;;============================================================================
;; let*

;(let* ((c 1) (x c))
;	(println c)
;	(println ($+ c x)))

;1
;2	

;;;============================================================================
;; letrec

;(letrec ((a (lambda (n)
;							(println ($* n 10))))
;				(b (lambda (n) 
;							(println n) (a n))))
;	(b 3))

;3
;30

;;---


;(println ($length '(1 2 3)))

;(letrec ((loop (lambda (n)
;									(println n)
;									(if ($<= n 0)
;										'()
;										(loop ($- n 1))))))
;			(loop 3))		

;3
;2
;1
;0

;;---

;;#!VAR

;(define f 4)
;(define g 6)

;(define (even? n)
;	(letrec ((f (lambda (n) (if ($= n 0) #t (g ($- n 1)))))
;					(g (lambda (n) (if ($= n 0) #f (f ($- n 1))))))
;		(f n)))

;	(letrec ((f (lambda (n) (if ($= n 0) #t (g ($- n 1)))))
;					(g (lambda (n) (if ($= n 0) #f (f ($- n 1))))))
;		(println (f 11)))

;(println (even? 5))

;(println g)

;4
;2
;0
;#t


;;---


;;#!VAR

;(define (boucle n)
;	(letrec ((loop (lambda (n)
;									(if ($<= n 0)
;										'()
										;(println #f)
;										(begin
											;(println n)
;											(loop ($- n 1)))))))
;		(loop n)))
	
;(boucle 6)
	

;;;============================================================================
;; let nommé

;(let fact ((n 5))
;	(println n)
;	(if ($= n 0)
;		1
;		($* n (fact ($- n 1)))))

;;---

;(let loop ((a 1) (b 1))
;	(println a)
;	(println b)
;	(println #t)
;	(if ($= a 4)
;		'()
;		(loop ($+ a 1) ($* b 2))))
		

;(let ((loop ($cons #f '())))
;	($set-car! loop
;		(lambda (n)
;			(println n)
;			(if ($= n 4)
;				(println #t)
;				'())
;			(println n)
;			(if ($= n 0)
;				'()
;				(($car loop) ($- n 1)))))
;	(($car loop) 6))

;(let ((loop ($cons #f '())))
;	($set-car! loop
;		(lambda (n)
;			(println n)
;			(if ($= n 0)
;				'()
;				(($car loop) ($- n 1)))))
;	(($car loop) 5))


;(let ((loop (lambda (n) ($+ n 1))))
;	(println (loop 2)))

;(let ((loop (cons #f '())))
;	(set-car! loop (lambda (n) ((car loop) (- n 1))))
;	(println (car loop))
; ((car loop) 5))

;;;============================================================================
;; let + define

;#!VAR

;(let ((c 1))
;	(define x 3)
;	(define y 4)
;	($set! x 4)
;	(println ($+ ($+ x y) c)))

; 9

;;---

;(let ((a 1))
;	(define (foo b) ($+ b b))
;	(println ($+ a (foo 4))))

;9

;;---

;; ne fonctionne pas

;(define (even n)
;	(define (f n) (if ($= n 0) #t (g ($- n 1))))
;	(define (g n) (if ($= n 0) #f (f ($- n 1))))
;	(f n))

;(println (even 5))


;;---	
	
;(let ()
;	(define x 4)
;	(define y 5)
;	(println ($+ y x)))

;9

;;;============================================================================
;; delay + force






















;;;============================================================================
;; a trier


;(println (+ (* 3 3) (quotient 100 25)))

;(let ((x (+ 4 4)))
;	(let ((y (+ 2 2)))
;		(println (+ x y))))


;(let ((x 2)) (println (+ x 1)))
;(let ((a 1) (b 3)) (println (+ a b)))
;(println 2)
;(println (+ 2 2))

;(if (and (= 0 1) (or #t #t))
;  (println #t)
;	(println #f))


;(let ((y 2) (x 6))
;	(println (* (let ((x 4)) x) y)))

;(let ((x  7) (y -3)) (println (* x y)))
;(let ((a -7) (b -3)) (println (* a b)))

;(let ((x 11))
;  (let ((x (+ x x)))
;    (let ((y (let ((x 2200))
;               (+ x x))))
;      (println (+ x y)))))

;(let ((x 2)) (println x))      
;(let ((a  7) (b  3)) (println (* a b)))

;(println (* 
;					(let ((x 1)) (+ x 1))
;          (let ((x 10)) (- x 1))))

;(let ((x (read-u8)))
;  (println (* (let ((x (read-u8)))
;                (+ x 2))
;              (let ((x (read-u8)))
;                (- x 1)))))

;(define pipi 2)
;(define pipi 3)
;(println pipi)

;(println (read-u8))
;(println #\g)
;(println '())
;(if (modulo 13 -4) (println #\a) (println #f))
;(define plus (lambda (a b) (+ a b)))
;(define power (lambda (x) (* x x)))
;(println (plus 1 (power 3)))




;(println (+ (- (* (quotient 10 (modulo 7 5)) 4) 10) 2))
;(println (>= (+ 3 (* 8 0)) (modulo -4 3)))

;(let ((x (+ 2 0)) (y (* 4 2)))
;	(let ((z (+ 20 0)))
;		(println (quotient z x))
;		(let ((w x))
;			(println w))))

;(println 1)
;(println (if (> (read-u8) 39) #t 123))

;(println (not (= 0 0)))
;(println (+ 1 1))

; (and) ; = #t
; (println (and #t)) ; = X
; (and X Y) ; = (if X Y #f)
; (and X Y Z) ; = (if X (and Y Z) #f)
;(println (and #f #t #t #t #t #f))

;(println (let ((x 2) (y 2))
;					(println (+ x y))
;					(+ x x)
;					(let ((z 3))
;						(println z)
;						(* z 2))))


;(println (let ((x (+ 2 0))) (+ x 2)))
;(let ((x 2) (y 3)) (println (< y x)))
;(let ((x (+ 1 1))) (println x))
;(println (+ 2 1))

;(println (and #t (println 123)))

;; ADD TESTS
;(+ 1 1)
;(println 1)
;(println (+ (+ 1 1) 1))
;(println (modulo 8 2))

;; PRINT TESTS

;(println #f)
;(println #t)
;(println #f)
;(println (println 2))

;; --------

;; IF TESTS

;(if (= 0 0)
;	(println #t)
;	(println #f))

;(if (= 2 0)
;	(println #t)
;	(if (= 0 0)
;		(println #t)
;		(println #f)))

;; --------

;; AND , OR , NOT TEST

;(let ((x (+ 2 0)) (y (* 4 2)))
;    (let ((z (+ 20 0)))
;        (println (quotient z x))
;        (let ((w x))
;            (println w))))

;(let ((a (read-u8)))
;(let ((b (read-u8)))
;(let ((x (if (< a 32) (- 100 a) a)))
;(println a)
;(println b)
;(println (+ x (* b b))))))


;(println (not (= 0 0)))
;(println (not (not (not (= 1 0)))))
	
;(println (and (= 0 0) (= 0 0)))
;(println (and (= 1 0) (= 0 0)))
;(println (and #t #f))
;(println (and #f 42))
;(println (and 10 42))
;(and (println 4) (not #f))
;(println (and 4 (or #t (not #t))))

;(println (and #f (println 100)))
;println (and -1 (println 100)))

;(println (or (= 1 0) (= 0 0)))
;(println (or (= 1 0) (= 0 1)))
;(println (or #t #f))
;(println (or (= 1 0) #t))
;(println (or (= 1 0) (println 100)))

;(if (and (= 0 0) (= 0 0))
;	(println #t)
;	(println #f))

;(if (not (and (= 0 0) (or (= 1 0) (= 0 0))))
;  (println #t)
;	(println #f))

;(println (if (= 1 0) (= 0 0) #f))
;(println (and (= 1 0) (= 0 0)))

;(if (< 1 4)
;	(if (> 6 7)
;		(println #t)
;		(if (= (modulo 4 2) 1)
;			(println #t)
;			(println #f)))
;	(println #f))



;; --------

;; LET TESTS

;(println (let ((x 4) (y 2))	(+ x (* y 2))))
		
;(let ((a 9))
;  (let ((b a))
;  	(+ a a)
;    (let ((c (let ((a 4)) (- b a))))
;      (println c))))
;(println (* 1 1))

;(println 
;	(let ((x 6) (y 2))
;		(println (+ y x))
;	))

;(println (let ((x 20))
;	(* 2 2)
;	(let ((x 44))
;		(println x))
;	(* 4 4)))
	
;	(let ((z x)) z)))
	
	
;(let ((x (+ 4 4)))
;	(let ((y (+ 2 2)))
;		(println (+ x y))))


;(println (>= 1 1))










