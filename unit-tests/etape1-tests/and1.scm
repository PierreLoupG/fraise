(println (and #f #f))
(println (and #f 42))
(println (and #t 42))
(println (and 10 42))

(and #f (println 111))
(and #t (println 222))
(and 10 (println 333))

;#f
;#f
;42
;42
;222
;333
