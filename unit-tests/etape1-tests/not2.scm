(let ((a #f)) (println (not a)))
(let ((a #t)) (println (not a)))
(let ((a 42)) (println (not a)))

;#t
;#f
;#f
