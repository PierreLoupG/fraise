(println
 (if (or (> (read-u8) 0) (> (read-u8) 0))
     (* (read-u8) 1000)
     (- (read-u8) 1)))

(println
 (if (or (< (read-u8) 0) (> (read-u8) 0))
     (* (read-u8) 1000)
     (- (read-u8) 1)))

(println
 (if (or (< (read-u8) 0) (< (read-u8) 0))
     (* (read-u8) 1000)
     (- (read-u8) 1)))

;98000
;101000
;103
