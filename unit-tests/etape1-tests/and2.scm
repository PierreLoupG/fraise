(println
 (if (and (> (read-u8) 0) (> (read-u8) 0))
     (* (read-u8) 1000)
     (- (read-u8) 1)))

(println
 (if (and (< (read-u8) 0) (> (read-u8) 0))
     (* (read-u8) 1000)
     (- (read-u8) 1)))

(println
 (if (and (< (read-u8) 0) (< (read-u8) 0))
     (* (read-u8) 1000)
     (- (read-u8) 1)))

;99000
;100
;102
