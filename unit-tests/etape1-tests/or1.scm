(println (or #f #f))
(println (or #f 42))
(println (or #t 42))
(println (or 10 42))

(or #f (println 111))
(or #t (println 222))
(or 10 (println 333))

;#f
;42
;#t
;10
;111
