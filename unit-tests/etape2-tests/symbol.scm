(println '#(0 foo #(cocolapin 2)))
(println 'foo)
(println 'bar)
(println '())
(println '(1 2 3))
(println 'foo)
(println '#(1 2 3))

;#(0foo#(cocolapin2))
;foo
;bar
;()
;(1 2 3)
;foo
;#(123)
