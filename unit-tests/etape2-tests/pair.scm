(define c ($cons 1 2))
(println ($car c))
(println ($cdr c))
($set-car! c 3)
($set-cdr! c 4)
(println ($car c))
(println ($cdr c))

;1
;2
;3
;4
