(let loop ((a 1) (b 1))
	(println a)
	(println b)
	(if (= a 4)
		'()
		(loop (+ a 1) (* b (+ b a)))))

;1
;1
;2
;2
;3
;8
;4
;88
