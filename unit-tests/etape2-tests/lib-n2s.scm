(define $number->string
  (lambda (n)
    (if ($< n 0)
        ($number->string-aux n 0 1)
        (if ($> n 0)
            ($number->string-aux ($- 0 n) 0 0)
            ($make-string 1 #\0)))))

(define $number->string-aux
  (lambda (n i extra)
    (if ($= n 0)
        ($make-string ($+ i extra) #\-)
        (let ((str ($number->string-aux ($quotient n 10) ($+ i 1) extra)))
          ($string-set! str
                        ($- ($string-length str) ($+ i 1))
                        ($integer->char ($- 48 ($remainder n 10))))
          str))))

(define $write-char
  (lambda (c)
    ($write-u8 ($char->integer c))))

(define $newline
  (lambda ()
    ($write-char ($integer->char 10))))

(define $display-boolean
  (lambda (b)
    ($write-char #\#)
    ($write-char (if b #\t #\f))))

(define $display-null
  (lambda ()
    ($write-char #\()
    ($write-char #\))))

(define $display-string
  (lambda (str)
    ($display-string-aux str 0)))

(define $display-string-aux
  (lambda (str i)
    (if ($< i ($string-length str))
        (begin
          ($write-char ($string-ref str i))
          ($display-string-aux str ($+ i 1)))
        #f)))

(define $display
  (lambda (x)
    (if ($eq? x #f)
        ($display-boolean x)
        (if ($eq? x #t)
            ($display-boolean x)
            (if ($eq? x '())
                ($display-null)
                (if ($number? x)
                    ($display-string ($number->string x))
                    (if ($string? x)
                        ($display-string x)
                        ($write-char #\?))))))))

(define println
  (lambda (x)
    ($display x)
    ($newline)))

(println #f)
(println #t)
(println '())
(println 0)
(println 1)
(println -1)
(println 9)
(println -9)
(println 10)
(println -10)
(println 1234567)
(println -1234567)
(println 576460752303423487)
(println -576460752303423488)

(println ($make-string 40 #\*))

;#f
;#t
;()
;0
;1
;-1
;9
;-9
;10
;-10
;1234567
;-1234567
;576460752303423487
;-576460752303423488
;****************************************
