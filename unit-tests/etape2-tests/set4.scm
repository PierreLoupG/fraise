(let ((a 3))
	((lambda (x)
		(set! a ($* x x))
		(println x)
		(println a)) 5))

;5
;25
