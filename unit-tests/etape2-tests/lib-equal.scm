(define $char-compare
  (lambda (c1 c2)
    (let ((n1 ($char->integer c1))
          (n2 ($char->integer c2)))
      (if ($< n1 n2)
          -1
          (if ($> n1 n2)
              +1
              0)))))

(define $char=?  (lambda (char1 char2) ($=  ($char-compare char1 char2) 0)))
(define $char<?  (lambda (char1 char2) ($<  ($char-compare char1 char2) 0)))
(define $char>?  (lambda (char1 char2) ($>  ($char-compare char1 char2) 0)))
(define $char<=? (lambda (char1 char2) ($<= ($char-compare char1 char2) 0)))
(define $char>=? (lambda (char1 char2) ($>= ($char-compare char1 char2) 0)))

(define $string-compare
  (lambda (str1 str2)
    (if ($eq? str1 str2)
        0
        ($string-compare-aux
         str1
         str2
         0
         (let ((len1 ($string-length str1))
               (len2 ($string-length str2)))
           (if ($< len1 len2) len1 len2))))))

(define $string-compare-aux
  (lambda (str1 str2 i len)
    (if ($< i len)
        (let ((c1 ($string-ref str1 i))
              (c2 ($string-ref str2 i)))
          (if ($char<? c1 c2)
              -1
              (if ($char>? c1 c2)
                  +1
                  ($string-compare-aux str1 str2 ($+ i 1) len))))
        (let ((len1 ($string-length str1))
              (len2 ($string-length str2)))
          (if ($< len1 len2)
              -1
              (if ($> len1 len2)
                  +1
                  0))))))

(define $string=?  (lambda (str1 str2) ($=  ($string-compare str1 str2) 0)))
(define $string<?  (lambda (str1 str2) ($<  ($string-compare str1 str2) 0)))
(define $string>?  (lambda (str1 str2) ($>  ($string-compare str1 str2) 0)))
(define $string<=? (lambda (str1 str2) ($<= ($string-compare str1 str2) 0)))
(define $string>=? (lambda (str1 str2) ($>= ($string-compare str1 str2) 0)))

(define $vector=?
  (lambda (vec1 vec2)
    (let ((len1 ($vector-length vec1))
          (len2 ($vector-length vec2)))
      (and ($= len1 len2)
           ($vector=?-aux vec1 vec2 ($- len1 1))))))

(define $vector=?-aux
  (lambda (vec1 vec2 i)
    (or ($< i 0)
        (let ((elem1 ($vector-ref vec1 i))
              (elem2 ($vector-ref vec2 i)))
          (and ($equal? elem1 elem2)
               ($vector=?-aux vec1 vec2 ($- i 1)))))))

(define $eqv?
  (lambda (obj1 obj2)
    (if ($number? obj1)
        (and ($number? obj2)
             ($= obj1 obj2))
        ($eq? obj1 obj2))))

(define $equal?
  (lambda (obj1 obj2)
    (if ($number? obj1)
        (and ($number? obj2)
             ($= obj1 obj2))
        (if ($pair? obj1)
            (and ($pair? obj2)
                 ($equal? ($car obj1) ($car obj2))
                 ($equal? ($cdr obj1) ($cdr obj2)))
            (if ($vector? obj1)
                (and ($vector? obj2)
                     ($vector=? obj1 obj2))
                (if ($string? obj1)
                    (and ($string? obj2)
                         ($string=? obj1 obj2))
                    ($eq? obj1 obj2)))))))

(define $member
  (lambda (x lst)
    (and ($pair? lst)
         (if ($equal? x ($car lst))
             lst
             ($member x ($cdr lst))))))

(define $assoc
  (lambda (key alist)
    (and ($pair? alist)
         (let ((x ($car alist)))
           (if ($equal? key ($car x))
               x
               ($assoc key ($cdr alist)))))))

($println ($eqv? 11 22))
($println ($eqv? 22 22))
($println ($eqv? 22 ($make-string 2 #\2)))
($println ($eqv? ($make-string 2 #\x) ($make-string 2 #\x)))
($println ($eqv? ($make-string 2 #\x) ($make-string 3 #\x)))
($println ($eqv? ($make-string 2 #\x) ($make-string 2 #\y)))
($println ($eqv? ($make-vector 2 11) ($make-vector 2 11)))
($println ($eqv? ($make-vector 2 11) ($make-vector 3 11)))
($println ($eqv? ($make-vector 2 11) ($make-vector 2 22)))

($println ($eqv? ($cons 11 ($cons 22 ($cons 33 '())))
                 ($cons 11 ($cons 22 ($cons 33 '())))))

($println ($eqv? ($cons 11 ($cons 22 '()))
                 ($cons 11 ($cons 22 ($cons 33 '())))))

($println ($equal? 11 22))
($println ($equal? 22 22))
($println ($equal? 22 ($make-string 2 #\2)))
($println ($equal? ($make-string 2 #\x) ($make-string 2 #\x)))
($println ($equal? ($make-string 2 #\x) ($make-string 3 #\x)))
($println ($equal? ($make-string 2 #\x) ($make-string 2 #\y)))
($println ($equal? ($make-vector 2 11) ($make-vector 2 11)))
($println ($equal? ($make-vector 2 11) ($make-vector 3 11)))
($println ($equal? ($make-vector 2 11) ($make-vector 2 22)))

($println ($equal? ($cons 11 ($cons 22 ($cons 33 '())))
                   ($cons 11 ($cons 22 ($cons 33 '())))))

($println ($equal? ($cons 11 ($cons 22 '()))
                   ($cons 11 ($cons 22 ($cons 33 '())))))

(define lst1
  ($cons 2 ($cons 3 ($cons 5 ($cons 7 '())))))

(define lst2
  ($cons ($cons 11 111)
         ($cons ($cons 22 222)
                ($cons ($cons 33 333)
                       '()))))

($println ($member 4 lst1))
($println ($car ($member 5 lst1)))

($println ($assoc 99 lst2))
($println ($cdr ($assoc 22 lst2)))

;#f
;#t
;#f
;#f
;#f
;#f
;#f
;#f
;#f
;#f
;#f
;#f
;#t
;#f
;#t
;#f
;#f
;#t
;#f
;#f
;#t
;#f
;#f
;5
;#f
;222
