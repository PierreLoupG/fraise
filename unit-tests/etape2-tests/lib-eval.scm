
(define $assq
  (lambda (key alst)
    (if ($pair? alst)
        (let ((x ($car alst)))
          (if ($eq? key ($car x))
              x
              ($assq key ($cdr alst))))
        #f)))

(define $memq
  (lambda (x lst)
    (if ($pair? lst)
        (if ($eq? x ($car lst))
            lst
            ($memq x ($cdr lst)))
        #f)))

(define $append
  (lambda (lst1 lst2)
    (if ($pair? lst1)
        ($cons ($car lst1)
               ($append ($cdr lst1) lst2))
        lst2)))

(define $map
  (lambda (f lst)
    (if ($pair? lst)
        ($cons ($apply f ($cons ($car lst) '()))
               ($map f ($cdr lst)))
        '())))

(define $apply
  (lambda (proc args)
    (if ($procedure? proc)

        (if ($pair? args)
            (let ((arg1 ($car args))
                  (args ($cdr args)))
              (if ($pair? args)
                  (let ((arg2 ($car args))
                        (args ($cdr args)))
                    (if ($pair? args)
                        (let ((arg3 ($car args))
                              (args ($cdr args)))
                          (if ($pair? args)
                              (let ((arg4 ($car args))
                                    (args ($cdr args)))
                                (if ($pair? args)
                                    (let ((arg5 ($car args))
                                          (args ($cdr args)))
                                      (if ($pair? args)
                                          (let ((arg6 ($car args))
                                                (args ($cdr args)))
                                            (if ($pair? args)
                                                (begin
                                                  ($println "RUN TIME ERROR -- number of arguments exceeds implementation limit")
                                                  ($halt))
                                                (proc arg1 arg2 arg3 arg4 arg5 arg6)))
                                          (proc arg1 arg2 arg3 arg4 arg5)))
                                    (proc arg1 arg2 arg3 arg4)))
                              (proc arg1 arg2 arg3)))
                        (proc arg1 arg2)))
                  (proc arg1)))
            (proc))

        (if ($eval-closure? proc)
            (let ((state ($eval-closure-state proc)))
              (let ((n-code ($car state))
                    (rte ($cdr state)))
                (let ((n ($car n-code))
                      (code ($cdr n-code)))
                  ($eval-enter-closure args n rte code))))
            (begin
              ($println "RUN TIME ERROR -- call of a non-procedure")
              ($halt))))))

(define $string->number
  (lambda (str)
    (and ($> ($string-length str) 0)
         (let ((c ($string-ref str 0)))
           (let ((n ($string->number-aux
                     str
                     (if (or ($char=? c #\+) ($char=? c #\-)) 1 0))))
             (and n
                  (if ($char=? c #\-) n ($- 0 n))))))))

(define $string->number-aux
  (lambda (str i)
    (and ($< i ($string-length str))
         ($string->number-aux2 str i 0))))

(define $string->number-aux2
  (lambda (str i n)
    (if ($< i ($string-length str))
        (let ((d ($char->integer ($string-ref str i))))
          (and ($>= d 48)
               ($<= d 57)
               ($string->number-aux2 str ($+ i 1) ($- ($* 10 n) ($- d 48)))))
        n)))

(define $number->string
  (lambda (n)
    (if ($< n 0)
        ($number->string-aux n 0 1)
        (if ($< 0 n)
            ($number->string-aux ($- 0 n) 0 0)
            ($make-string 1 #\0)))))

(define $number->string-aux
  (lambda (n i extra)
    (if ($= n 0)
        ($make-string ($+ i extra) #\-)
        (let ((str ($number->string-aux ($quotient n 10) ($+ i 1) extra)))
          ($string-set! str
                        ($- ($string-length str) ($+ i 1))
                        ($integer->char ($- 48 ($remainder n 10))))
          str))))

(define $gensym-count 0)

(define $gensym
  (lambda ()
    (set! $gensym-count ($+ $gensym-count 1))
    ($string->symbol
     ($string-append "_g" ($number->string $gensym-count)))))

(define $string-append
  (lambda (str1 str2)
    (let ((len1 ($string-length str1))
          (len2 ($string-length str2)))
      (let ((result ($make-string ($+ len1 len2) #\_)))
        ($string-move! str1 0 result 0)
        ($string-move! str2 0 result len1)))))

(define $string-move!
  (lambda (str i dest j)
    (if ($< i ($string-length str))
        (begin
          ($string-set! dest j ($string-ref str i))
          ($string-move! str ($+ i 1) dest ($+ j 1)))
        dest)))

(define $halt
  (lambda ()
    ($halt)))

(define $match-failed
  (lambda (x)
    ($println "MATCH FAILED")
    ($halt)))

(define $eval-syntax-error
  (lambda (expr)
    ($println "SYNTAX ERROR")
    ($halt)))

(define $eval-wrong-nargs
  (lambda ()
    ($println "RUN TIME ERROR -- wrong number of arguments")
    ($halt)))

(define $eval-closure-marker
  ($cons 'closure '()))

(define $eval-procedure?
  (lambda (x)
    (or ($procedure? x)
        ($eval-closure? x))))

(define $eval-closure?
  (lambda (x)
    (and ($pair? x)
         ($eq? ($car x) $eval-closure-marker))))

(define $eval-make-closure
  (lambda (state)
    ($cons $eval-closure-marker state)))

(define $eval-closure-state
  (lambda (clo)
    ($cdr clo)))

(define $eval-non-closure-pair?
  (lambda (x)
    (and ($pair? x)
         ($not ($eq? ($car x) $eval-closure-marker)))))

(define $expand-and
  (lambda (expr)
    (let ((args ($cdr expr)))
      (if ($pair? args)
          (let ((rest ($cdr args)))
            (if ($pair? rest)
                ($cons 'if
                       ($cons ($car args)
                              ($cons ($cons 'and rest)
                                     ($cons #f
                                            '()))))
                ($car args)))
          #t))))

(define $expand-or
  (lambda (expr)
    (let ((args ($cdr expr)))
      (if ($pair? args)
          (let ((rest ($cdr args)))
            (if ($pair? rest)
                (let ((tmp ($gensym)))
                  ($cons 'let
                         ($cons ($cons ($cons tmp
                                              ($cons ($car args)
                                                     '()))
                                       '())
                                ($cons ($cons 'if
                                              ($cons tmp
                                                     ($cons tmp
                                                            ($cons ($cons 'or rest)
                                                                   '()))))
                                       '()))))
                ($car args)))
          #f))))

(define $expand-let
  (lambda (expr)
    (let ((args ($cdr expr)))
      (if (and ($pair? args)
               ($pair? ($cdr args)))
          ($cons ($cons 'lambda
                        ($cons ($map $binding-var
                                     ($car args))
                               ($cdr args)))
                 ($map $binding-expr
                       ($car args)))
          ($eval-syntax-error expr)))))

(define $expand-let*
  (lambda (expr)
    (let ((args ($cdr expr)))
      (if (and ($pair? args)
               ($pair? ($cdr args)))
          ($cons 'let
                 (if (and ($pair? ($car args))
                          ($pair? ($cdr ($car args))))
                     ($cons ($cons ($car ($car args)) '())
                            ($cons ($cons 'let*
                                          ($cons ($cdr ($car args))
                                                 ($cdr args)))
                                   '()))
                     args))
          ($eval-syntax-error expr)))))

(define $expand-match-if-equal?
  (lambda (expr var pattern yes no)
    (if (and ($pair? pattern)
             ($eq? ($car pattern) 'unquote)
             ($pair? ($cdr pattern))
             ($null? ($cdr ($cdr pattern))))
        ($cons 'let
               ($cons ($cons ($cons ($car ($cdr pattern))
                                    ($cons var
                                           '()))
                             '())
                      ($cons yes
                             '())))
        (if (or ($null? pattern)
                ($symbol? pattern)
                ($boolean? pattern)
                ($number? pattern)
                ($char? pattern)
                ($string? pattern))
            ($cons 'if
                   ($cons ($cons '$equal?
                                 ($cons var
                                        ($cons ($cons 'quote
                                                      ($cons pattern
                                                             '()))
                                               '())))
                          ($cons yes
                                 ($cons no
                                        '()))))
            (if ($pair? pattern)
                (let ((carvar ($gensym))
                      (cdrvar ($gensym)))
                  ($cons
                   'if
                   ($cons
                    ($cons '$pair?
                           ($cons var
                                  '()))
                    ($cons
                     ($cons
                      'let
                      ($cons
                       ($cons
                        ($cons carvar
                               ($cons ($cons '$car
                                             ($cons var
                                                    '()))
                                      '()))
                        '())
                       ($cons
                        ($expand-match-if-equal?
                         expr
                         carvar
                         ($car pattern)
                         ($cons 'let
                                ($cons ($cons ($cons cdrvar
                                                     ($cons ($cons '$cdr
                                                                   ($cons var
                                                                          '()))
                                                            '()))
                                              '())
                                       ($cons ($expand-match-if-equal?
                                               expr
                                               cdrvar
                                               ($cdr pattern)
                                               yes
                                               no)
                                              '())))
                         no)
                        '())))
                     ($cons no
                            '())))))
                ($eval-syntax-error expr))))))

(define $expand-match-clauses
  (lambda (expr var fns1 fns2 clauses body)
    (if ($pair? clauses)
        (let ((clause ($car clauses)))
          ($expand-match-clauses
           expr
           var
           ($cdr fns1)
           ($cdr fns2)
           ($cdr clauses)
           ($cons
            'let
            ($cons
             ($cons
              ($cons
               ($car fns1)
               ($cons
                ($cons
                 'lambda
                 ($cons
                  ($cons
                   var
                   '())
                  ($cons
                   ($expand-match-if-equal?
                    expr
                    var
                    ($car clause)
                    (if (and ($eq? ($car ($cdr clause)) 'when)
                             ($pair? ($cdr ($cdr clause))))
                        ($cons 'if
                               ($cons ($car ($cdr ($cdr clause)))
                                      ($cons ($car ($cdr ($cdr ($cdr clause))))
                                             ($cons ($cons ($car fns2)
                                                           ($cons var
                                                                  '()))
                                                    '()))))
                        ($car ($cdr clause)))
                    ($cons ($car fns2)
                           ($cons var
                                  '())))
                   '())))
                '()))
              '())
             ($cons body
                    '())))))
        body)))

(define $expand-match
  (lambda (expr)
    (let ((subject
           ($car ($cdr expr)))
          (clauses
           ($cdr ($cdr expr)))
          (var
           ($gensym))
          (err
           ($gensym)))
      (let ((fns
             ($append ($map (lambda (x) ($gensym))
                            clauses)
                      ($cons err
                             '()))))
        ($cons
         'let
         ($cons
          ($cons ($cons var
                        ($cons subject
                               '()))
                 '())
          ($cons
           ($cons
            'let
            ($cons
             ($cons
              ($cons err
                     ($cons ($cons 'lambda
                                   ($cons ($cons var
                                                 '())
                                          ($cons ($cons '$match-failed
                                                        ($cons var
                                                               '()))
                                                 '())))
                            '()))
              '())
             ($cons ($expand-match-clauses
                     expr
                     var
                     fns
                     ($cdr fns)
                     clauses
                     ($cons ($car fns)
                            ($cons var
                                   '())))
                    '())))
           '())))))))

(define $expand-define
  (lambda (expr)
    ($eval-syntax-error expr)))

(define $expand-define-macro
  (lambda (expr)
    ($eval-syntax-error expr)))

(define $binding-var
  (lambda (x)
    (if ($pair? x)
        ($car x)
        ($eval-syntax-error x))))

(define $binding-expr
  (lambda (x)
    (if (and ($pair? x)
             ($pair? ($cdr x)))
        ($car ($cdr x))
        ($eval-syntax-error x))))

(define $eval
  (lambda (expr)
    ($eval-run ($eval-comp-top expr) ($eval-grte))))

(define $eval-run
  (lambda (code rte)
    (($car code) ($cdr code) rte)))

(define $eval-global-cte
  ($cons 0 ;; number of non-global variables in scope
         '()))

(define $eval-define-global-macro
  (lambda (name expander)
    ($set-cdr! $eval-global-cte
      ($cons ($cons name expander) ($cdr $eval-global-cte)))))

(define $eval-gcte
  (lambda ()
    $eval-global-cte))

(define $cte-fs
  (lambda (cte)
    ($car cte)))

(define $cte-lookup
  (lambda (cte name)
    ($assq name ($cdr cte))))

(define $eval-global-rte
  '())

(define $eval-define-global-var
  (lambda (name val)
    (set! $eval-global-rte
      ($cons ($cons name val) $eval-global-rte))))

(define $eval-grte
  (lambda ()
    $eval-global-rte))

(define $rte-loc-ref
  (lambda (rte i)
    (if ($= i 0)
        ($car rte)
        ($rte-loc-ref ($cdr rte) ($- i 1)))))

(define $rte-loc-set!
  (lambda (rte i val)
    (if ($= i 0)
        ($set-car! rte val)
        ($rte-loc-set! ($cdr rte) ($- i 1) val))))

(define $rte-glo-ref
  (lambda (rte name)
    (let ((x ($car rte)))
      (if ($eq? ($car x) name)
          ($cdr x)
          (let ((rest ($cdr rte)))
            (if ($pair? rest)
                ($rte-glo-ref rest name)
                (begin
                  ($println "RUN TIME ERROR -- unbound global variable:")
                  ($println ($symbol->string name))
                  ($halt))))))))

(define $rte-glo-set!
  (lambda (rte name val)
    (let ((x ($car rte)))
      (if ($eq? ($car x) name)
          ($set-cdr! x val)
          (let ((rest ($cdr rte)))
            (if ($pair? rest)
                ($rte-glo-set! rest name val)
                ($set-cdr! rte ($cons ($cons name val) '()))))))))

(define $eval-comp-top
  (lambda (expr)
    (if ($pair? expr)
        (let ((form ($car expr))
              (rest ($cdr expr)))
          (if ($eq? form 'begin)
              ($eval-comp-top-begin rest)
              (if (or ($eq? form 'define)
                      ($eq? form 'define-macro))
                  ($eval-comp-top-define expr)
                  ($eval-comp expr ($eval-gcte)))))
        ($eval-comp expr ($eval-gcte)))))

(define $eval-comp-top-begin
  (lambda (rest)
    (if ($pair? rest)
        (if ($pair? ($cdr rest))
            (let ((code ($eval-comp-top ($car rest))))
              ($eval-gen-seq code ($eval-comp-top-begin ($cdr rest))))
            ($eval-comp-top ($car rest)))
        ($eval-gen-lit #f))))

(define $eval-comp-top-define
  (lambda (expr)
    (let ((rest ($cdr expr)))
      (if (and ($pair? rest)
               ($pair? ($cdr rest)))
          (let ((pattern ($car rest)))
            (if (and ($pair? pattern)
                     ($symbol? ($car pattern)))
                ($eval-comp-top-define-aux expr
                                           ($car pattern)
                                           ($cons 'lambda
                                                  ($cons ($cdr pattern)
                                                         ($cdr rest))))
                (if ($null? ($cdr ($cdr rest)))
                    ($eval-comp-top-define-aux expr
                                               pattern
                                               ($car ($cdr rest)))
                    ($eval-syntax-error expr))))
          ($eval-syntax-error expr)))))

(define $eval-comp-top-define-aux
  (lambda (expr name expr2)
    (if ($symbol? name)
        (if ($eq? ($car expr) 'define)
            ($eval-gen-glo-set! name ($eval-comp expr2 ($eval-gcte)))
            (begin
              ($eval-define-global-macro
               name
               ($eval
                ($cons 'lambda
                       ($cons ($cons 'expr '())
                              ($cons ($cons 'apply
                                            ($cons expr2
                                                   ($cons ($cons 'cdr
                                                                 ($cons 'expr
                                                                        '()))
                                                          '())))
                                     '())))))
              ($eval-gen-lit #f)))
        ($eval-syntax-error expr))))

(define $eval-comp
  (lambda (expr cte)
    (if ($eval-to-self? expr)
        ($eval-gen-lit expr)
        (if ($symbol? expr)
            ($eval-comp-ref expr cte)
            (if ($pair? expr)
                (let ((form ($car expr)))
                  (let ((x ($cte-lookup cte form)))
                    (if (and x
                             ($eval-procedure? ($cdr x)))
                        ($eval-comp ($apply ($cdr x) ($cons expr '())) cte)
                        (if ($eq? form 'begin)
                            ($eval-comp-begin ($cdr expr) cte)
                            (if ($eq? form 'lambda)
                                ($eval-comp-lambda expr cte)
                                (if ($eq? form 'if)
                                    ($eval-comp-if expr cte)
                                    (if ($eq? form 'set!)
                                        ($eval-comp-set! expr cte)
                                        (if ($eq? form 'quote)
                                            ($eval-comp-quote expr cte)
                                            ($eval-comp-call expr cte)))))))))
                ($eval-syntax-error expr))))))

(define $eval-to-self?
  (lambda (x)
    (or ($number? x)
        ($boolean? x)
        ($char? x)
        ($vector? x)
        ($string? x))))

(define $eval-comp-quote
  (lambda (expr cte)
    (if (and ($pair? expr)
             ($pair? ($cdr expr))
             ($null? ($cdr ($cdr expr))))
        ($eval-gen-lit ($car ($cdr expr)))
        ($eval-syntax-error expr))))

(define $eval-comp-ref
  (lambda (expr cte)
    (let ((x ($cte-lookup cte expr)))
      (if x
          (if ($number? ($cdr x))
              ($eval-gen-loc-ref ($- ($- ($cte-fs cte) ($cdr x)) 1))
              ($eval-syntax-error expr))
          ($eval-gen-glo-ref expr)))))

(define $eval-comp-set!
  (lambda (expr cte)
    (let ((rest ($cdr expr)))
      (if (and ($pair? rest)
               ($pair? ($cdr rest))
               ($null? ($cdr ($cdr rest))))
          (let ((var ($car rest)))
            (if ($symbol? var)
                (let ((x ($cte-lookup cte var))
                      (code ($eval-comp ($car ($cdr rest)) cte)))
                  (if x
                      (if ($number? ($cdr x))
                          ($eval-gen-loc-set! ($- ($- ($cte-fs cte) ($cdr x)) 1) code)
                          ($eval-syntax-error expr))
                      ($eval-gen-glo-set! var code)))
                ($eval-syntax-error expr)))
          ($eval-syntax-error expr)))))

(define $eval-comp-if
  (lambda (expr cte)
    (let ((rest ($cdr expr)))
      (if (and ($pair? rest)
               ($pair? ($cdr rest))
               ($pair? ($cdr ($cdr rest)))
               ($null? ($cdr ($cdr ($cdr rest)))))
          (let ((code1 ($eval-comp ($car rest) cte)))
            (let ((code2 ($eval-comp ($car ($cdr rest)) cte)))
              (let ((code3 ($eval-comp ($car ($cdr ($cdr rest))) cte)))
                ($eval-gen-if code1 code2 code3))))
          ($eval-syntax-error expr)))))

(define $eval-comp-begin
  (lambda (rest cte)
    (if ($pair? rest)
        (if ($pair? ($cdr rest))
            (let ((code ($eval-comp ($car rest) cte)))
              ($eval-gen-seq code ($eval-comp-begin ($cdr rest) cte)))
            ($eval-comp ($car rest) cte))
        ($eval-gen-lit #f))))

(define $eval-comp-lambda
  (lambda (expr cte)
    (let ((rest ($cdr expr)))
      (if (and ($pair? rest)
               ($pair? ($cdr rest)))
          (let ((params ($car rest)))
            (let ((n ($eval-param-list-length expr params '())))
              (let ((cte2 ($cte-extend-with-param-list cte params)))
                (let ((code ($eval-comp-begin ($cdr rest) cte2)))
                  ($eval-gen-closure n code)))))
          ($eval-syntax-error expr)))))

(define $eval-comp-call
  (lambda (expr cte)
    (let ((codes ($eval-comp-call-aux expr cte)))
      ($eval-gen-call codes))))

(define $eval-comp-call-aux
  (lambda (exprs cte)
    (if ($pair? exprs)
        ($cons ($eval-comp ($car exprs) cte)
               ($eval-comp-call-aux ($cdr exprs) cte))
        '())))

(define $eval-param-list-length
  (lambda (expr params seen)
    (if ($null? params)
        0
        (if ($pair? params)
            (let ((param ($car params)))
              (if ($symbol? param)
                  (if ($memq param seen)
                      ($eval-syntax-error expr)
                      (let ((n ($eval-param-list-length expr
                                                        ($cdr params)
                                                        ($cons param seen))))
                        (if ($< n 0)
                            ($- n 1)
                            ($+ n 1))))
                  ($eval-syntax-error expr)))
            (if (and ($symbol? params)
                     ($not ($memq params seen)))
                -1
                ($eval-syntax-error expr))))))

(define $cte-extend-with-param-list
  (lambda (cte params)
    (if ($null? params)
        cte
        (if ($pair? params)
            (let ((cte2 ($cte-extend-with-param-list cte ($cdr params))))
              (let ((param ($car params))
                    (fs ($car cte2)))
                ($cons ($+ fs 1) ($cons ($cons param fs) ($cdr cte2)))))
            (let ((fs ($car cte)))
              ($cons ($+ fs 1) ($cons ($cons params fs) ($cdr cte))))))))

(define $eval-gen-lit
  (lambda (val)
    ($cons (lambda (val rte)
             val)
           val)))

(define $eval-gen-loc-ref
  (lambda (i)
    ($cons (lambda (i rte)
             ($rte-loc-ref rte i))
           i)))

(define $eval-gen-loc-set!
  (lambda (i code)
    ($cons (lambda (i-code rte)
             ($rte-loc-set! rte
                            ($car i-code)
                            ($eval-run ($cdr i-code) rte)))
           ($cons i code))))

(define $eval-gen-glo-ref
  (lambda (name)
    ($cons (lambda (name rte)
             ($rte-glo-ref ($eval-grte) name))
           name)))

(define $eval-gen-glo-set!
  (lambda (name code)
    ($cons (lambda (name-code rte)
             ($rte-glo-set! ($eval-grte)
                            ($car name-code)
                            ($eval-run ($cdr name-code) rte)))
           ($cons name code))))

(define $eval-gen-if
  (lambda (code1 code2 code3)
    ($cons (lambda (codes rte)
             (if ($eval-run ($car codes) rte)
                 ($eval-run ($car ($cdr codes)) rte)
                 ($eval-run ($cdr ($cdr codes)) rte)))
           ($cons code1 ($cons code2 code3)))))

(define $eval-gen-seq
  (lambda (code1 code2)
    ($cons (lambda (codes rte)
             ($eval-run ($car codes) rte)
             ($eval-run ($cdr codes) rte))
           ($cons code1 code2))))

(define $eval-gen-closure
  (lambda (n code)
    ($cons (lambda (n-code rte)
             ($eval-make-closure ($cons n-code rte)))
           ($cons n code))))

(define $eval-enter-closure
  (lambda (args n rte code)
    (if ($= n -1)
        ($eval-run code ($cons args rte))
        (if ($= n 0)
            (if ($null? args)
                ($eval-run code rte)
                ($eval-wrong-nargs))
            (begin
              ($eval-enter-closure-aux args n rte)
              ($eval-run code args))))))

(define $eval-enter-closure-aux
  (lambda (args n rte)
    ;; n>=1 or n<=-2
    (if ($pair? args)
        (if ($< n 0)
            (if ($= n -2)
                ($set-cdr! args ($cons ($cdr args) rte))
                ($eval-enter-closure-aux ($cdr args) ($+ n 1) rte))
            (if ($= n 1)
                (if ($null? ($cdr args))
                    ($set-cdr! args rte)
                    ($eval-wrong-nargs))
                ($eval-enter-closure-aux ($cdr args) ($- n 1) rte)))
        ($eval-wrong-nargs))))

(define $eval-gen-call
  (lambda (codes)
    ($cons (lambda (codes rte)
             (let ((x ($eval-run* codes rte)))
               ($apply ($car x) ($cdr x))))
           codes)))

(define $eval-run*
  (lambda (codes rte)
    (if ($pair? codes)
        ($cons ($eval-run ($car codes) rte)
               ($eval-run* ($cdr codes) rte))
        '())))

;; special forms implemented as source-to-source transformations:
($eval-define-global-macro 'and          $expand-and)
($eval-define-global-macro 'or           $expand-or)
($eval-define-global-macro 'let          $expand-let)
($eval-define-global-macro 'let*         $expand-let*)
($eval-define-global-macro 'match        $expand-match)
($eval-define-global-macro 'define       $expand-define)
($eval-define-global-macro 'define-macro $expand-define-macro)

;; for expansion of match form:
($eval-define-global-var '$match-failed  $match-failed)
($eval-define-global-var '$pair?         $eval-non-closure-pair?)
($eval-define-global-var '$car           (lambda (x) ($car x)))
($eval-define-global-var '$cdr           (lambda (x) ($cdr x)))
($eval-define-global-var '$equal?        (lambda (x y) ($equal? x y)))

;; standard procedures:
($eval-define-global-var 'eq?            (lambda (x y) ($eq? x y)))
($eval-define-global-var 'equal?         (lambda (x y) ($equal? x y)))

($eval-define-global-var 'null?          (lambda (x) ($null? x)))

($eval-define-global-var 'boolean?       (lambda (x) ($boolean? x)))
($eval-define-global-var 'not            (lambda (x) ($not x)))

($eval-define-global-var 'procedure?     $eval-procedure?)

($eval-define-global-var 'pair?          $eval-non-closure-pair?)
($eval-define-global-var 'cons           (lambda (x y) ($cons x y)))
($eval-define-global-var 'car            (lambda (x) ($car x)))
($eval-define-global-var 'cdr            (lambda (x) ($cdr x)))
($eval-define-global-var 'set-car!       (lambda (x y) ($set-car! x y)))
($eval-define-global-var 'set-cdr!       (lambda (x y) ($set-cdr! x y)))

($eval-define-global-var 'number?        (lambda (x) ($number? x)))
($eval-define-global-var '+              (lambda (x y) ($+ x y)))
($eval-define-global-var '-              (lambda (x y) ($- x y)))
($eval-define-global-var '*              (lambda (x y) ($* x y)))
($eval-define-global-var 'quotient       (lambda (x y) ($quotient x y)))
($eval-define-global-var 'remainder      (lambda (x y) ($remainder x y)))
($eval-define-global-var '=              (lambda (x y) ($= x y)))
($eval-define-global-var '<              (lambda (x y) ($< x y)))
($eval-define-global-var '>              (lambda (x y) ($> x y)))
($eval-define-global-var '<=             (lambda (x y) ($<= x y)))
($eval-define-global-var '>=             (lambda (x y) ($>= x y)))

($eval-define-global-var 'char?          (lambda (x) ($char? x)))
($eval-define-global-var 'integer->char  (lambda (x) ($integer->char x)))
($eval-define-global-var 'char->integer  (lambda (x) ($char->integer x)))
($eval-define-global-var 'char=?         (lambda (x y) ($char=? x y)))
($eval-define-global-var 'char<?         (lambda (x y) ($char<? x y)))
($eval-define-global-var 'char>?         (lambda (x y) ($char>? x y)))
($eval-define-global-var 'char<=?        (lambda (x y) ($char<=? x y)))
($eval-define-global-var 'char>=?        (lambda (x y) ($char>=? x y)))

($eval-define-global-var 'string?        (lambda (x) ($string? x)))
($eval-define-global-var 'make-string    (lambda (x y) ($make-string x y)))
($eval-define-global-var 'string-length  (lambda (x) ($string-length x)))
($eval-define-global-var 'string-ref     (lambda (x y) ($string-ref x y)))
($eval-define-global-var 'string-set!    (lambda (x y z) ($string-set! x y z)))

($eval-define-global-var 'vector?        (lambda (x) ($vector? x)))
($eval-define-global-var 'make-vector    (lambda (x y) ($make-vector x y)))
($eval-define-global-var 'vector-length  (lambda (x) ($vector-length x)))
($eval-define-global-var 'vector-ref     (lambda (x y) ($vector-ref x y)))
($eval-define-global-var 'vector-set!    (lambda (x y z) ($vector-set! x y z)))

($eval-define-global-var 'symbol?        (lambda (x) ($symbol? x)))
($eval-define-global-var 'string->symbol (lambda (x) ($string->symbol x)))
($eval-define-global-var 'symbol->string (lambda (x) ($symbol->string x)))

($eval-define-global-var 'read-u8        (lambda () ($read-u8)))
($eval-define-global-var 'write-u8       (lambda (x) ($write-u8 x)))
($eval-define-global-var 'println        (lambda (x) ($println x)))

($eval-define-global-var 'assq           $assq)
($eval-define-global-var 'memq           $memq)
($eval-define-global-var 'append         $append)
($eval-define-global-var 'map            $map)
($eval-define-global-var 'apply          $apply)
($eval-define-global-var 'number->string $number->string)
($eval-define-global-var 'string->number $string->number)
($eval-define-global-var 'gensym         $gensym)
($eval-define-global-var 'string-append  $string-append)

($eval-define-global-var 'eval           $eval)

($eval '(define list (lambda lst lst)))

;; tests

($println ($eval 42))
($println ($eval #t))
($println ($eval #f))
($println ($eval '(quote 123)))
($println ($eval '(begin)))
($println ($eval '(begin 111)))
($println ($eval '(begin 111 222)))
($println ($eval '(begin 111 222 333)))
($eval '(define n 567))
($println ($eval 'n))
($eval '(set! n 42))
($println ($eval 'n))
($println ($eval '(if #t 11 22)))
($println ($eval '(if #f 11 22)))
($println ($eval '((lambda () 1000))))
($println ($eval '((lambda (a) (+ 1000 a)) 11)))
($println ($eval '((lambda (a b) (+ (+ 1000 a) b)) 11 22)))
($println ($eval '((lambda a (if (pair? a) (car a) 555)))))
($println ($eval '((lambda a (if (pair? a) (car a) 555)) 11)))
($println ($eval '((lambda a (if (pair? a) (car a) 555)) 11 22)))
($println ($eval '((lambda a (if (pair? a) (car a) 555)) 11 22 33)))
($println ($eval '((lambda (a . b) (if (pair? b) (car b) a)) 11)))
($println ($eval '((lambda (a . b) (if (pair? b) (car b) a)) 11 22)))
($println ($eval '((lambda (a . b) (if (pair? b) (car b) a)) 11 22 33)))
($println ($eval '(and)))
($println ($eval '(and 42)))
($println ($eval '(and #f)))
($println ($eval '(and #f 100)))
($println ($eval '(and 42 #f)))
($println ($eval '(and 42 100)))
($println ($eval '(and #f 100 200)))
($println ($eval '(and 42 #f 200)))
($println ($eval '(and 42 100 #f)))
($println ($eval '(and 42 100 200)))
($println ($eval '(or)))
($println ($eval '(or 42)))
($println ($eval '(or #f)))
($println ($eval '(or #f 100)))
($println ($eval '(or 42 #f)))
($println ($eval '(or 42 100)))
($println ($eval '(or #f 100 200)))
($println ($eval '(or 42 #f 200)))
($println ($eval '(or 42 100 #f)))
($println ($eval '(or 42 100 200)))
($println ($eval '(let () (+ 100 200))))
($println ($eval '(let ((a 11)) (+ 100 a))))
($println ($eval '(let ((a 11) (b 22)) (+ a b))))
($println ($eval '(let* () (+ 100 200))))
($println ($eval '(let* ((a 11)) (+ 100 a))))
($println ($eval '(let* ((a 11) (b (+ a 1000))) (+ a b))))

($println ($eval '(match (list 1 2 3) ((1 2) 888) ((1 2 3) 999))))

($println ($eval '(eval '(+ 1 2))))

($eval '(define-macro (inc var) (list 'set! var (list '+ var 1))))

($eval '(inc n))

($println ($eval 'n))

($eval '(define (fib x)
          (if (< x 2)
              x
              (+ (fib (- x 1))
                 (fib (- x 2))))))

($eval '(println (fib 10)))

;42
;#t
;#f
;123
;#f
;111
;222
;333
;567
;42
;11
;22
;1000
;1011
;1033
;555
;11
;11
;11
;11
;22
;22
;#t
;42
;#f
;#f
;#f
;100
;#f
;#f
;#f
;200
;#f
;42
;#f
;100
;42
;42
;100
;42
;42
;42
;300
;111
;33
;300
;111
;1022
;999
;3
;43
;55
