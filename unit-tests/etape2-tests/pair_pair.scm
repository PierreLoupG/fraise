(define x ($cons 5 ($cons 6 7)))
(println ($car x))
(println ($car ($cdr x)))
(println ($cdr ($cdr x)))
($set-car! x 2)
(println ($car x))

;5
;6
;7
;2
