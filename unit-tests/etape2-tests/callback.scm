(define foo 
	(lambda (callback)
		(callback)
		(callback)
		(callback)))

(let ((i 0))
	(foo
		(lambda ()
			(set! i ($+ i 1))
			(println i))))
			
;1
;2
;3
