(define f
	(lambda (a b)
		(lambda (a t)
			(lambda (x y z)
				($+ x a)))))

(define h (f 1 2))
(define g (h 3 4))
(println (g 10 20 30))

;13
