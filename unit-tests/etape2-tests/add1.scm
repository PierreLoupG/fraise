(define add1
  (lambda (x)
    (+ x 1)))

(println (add1 (read-u8)))
(println (add1 (read-u8)))
(println (add1 (read-u8)))

;98
;99
;100
