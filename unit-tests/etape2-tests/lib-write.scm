(define $vector->list
  (lambda (vect)
    ($vector->list-aux vect 0)))

(define $vector->list-aux
  (lambda (vect i)
    (if ($< i ($vector-length vect))
        ($cons ($vector-ref vect i)
               ($vector->list-aux vect ($+ i 1)))
        '())))

(define $substring
  (lambda (str start end)
    (let ((len ($- end start)))
      (let ((result ($make-string len #\_)))
        ($string-move! str start end result 0)))))

(define $string-append
  (lambda (str1 str2)
    (let ((len1 ($string-length str1))
          (len2 ($string-length str2)))
      (let ((result ($make-string ($+ len1 len2) #\_)))
        ($string-move! str1 0 ($string-length str1) result 0)
        ($string-move! str2 0 ($string-length str2) result len1)))))

(define $string-move!
  (lambda (str i j dest k)
    (if ($< i j)
        (begin
          ($string-set! dest k ($string-ref str i))
          ($string-move! str ($+ i 1) j dest ($+ k 1)))
        dest)))

(define $string->number
  (lambda (str)
    (and ($> ($string-length str) 0)
         (let ((c ($string-ref str 0)))
           (let ((n ($string->number-aux
                     str
                     (if (or ($char=? c #\+) ($char=? c #\-)) 1 0))))
             (and n
                  (if ($char=? c #\-) n ($- 0 n))))))))

(define $string->number-aux
  (lambda (str i)
    (and ($< i ($string-length str))
         ($string->number-aux2 str i 0))))

(define $string->number-aux2
  (lambda (str i n)
    (if ($< i ($string-length str))
        (let ((d ($char->integer ($string-ref str i))))
          (and ($>= d 48)
               ($<= d 57)
               ($string->number-aux2 str ($+ i 1) ($- ($* 10 n) ($- d 48)))))
        n)))

(define $number->string
  (lambda (n)
    (if ($< n 0)
        ($number->string-aux n 0 1)
        (if ($< 0 n)
            ($number->string-aux ($- 0 n) 0 0)
            ($make-string 1 #\0)))))

(define $number->string-aux
  (lambda (n i extra)
    (if ($= n 0)
        ($make-string ($+ i extra) #\-)
        (let ((str ($number->string-aux ($quotient n 10) ($+ i 1) extra)))
          ($string-set! str
                        ($- ($string-length str) ($+ i 1))
                        ($integer->char ($- 48 ($remainder n 10))))
          str))))

(define $read-macro?
  (lambda (lst)
    (and ($pair? lst)
         ($length1? ($cdr lst))
         (if ($eq? ($car lst) 'quote)
             "'"
             (if ($eq? ($car lst) 'quasiquote)
                 "`"
                 (if ($eq? ($car lst) 'unquote)
                     ","
                     (if ($eq? ($car lst) 'unquote-splicing)
                         ",@"
                         #f)))))))

(define $length1?
  (lambda (lst)
    (and ($pair? lst)
         ($null? ($cdr lst)))))

(define $read-macro-body
  (lambda (lst)
    ($car ($cdr lst))))

(define $out
  (lambda (str col output)
    (and col
         (output str)
         ($+ col ($string-length str)))))

(define $spaces
  (lambda (n col output)
    (if ($> n 0)
        (if ($>= n 8)
            ($spaces ($- n 8) ($out "        " col output) output)
            ($spaces ($- n 1) ($out " " col output) output))
        col)))

(define $indent
  (lambda (align col output)
    (and col
         (if ($< align col)
             (and ($out "\n" col output)
                  ($spaces align 0 output))
             ($spaces ($- align col) col output)))))

(define $wr-sexpr
  (lambda (obj col style output)
    (if ($eq? style 'display)
        ($wr-list obj col style output)
        (let ((prefix ($read-macro? obj)))
          (if prefix
              ($wr ($read-macro-body obj) ($out prefix col output) style output)
              (if ($eq? style 'write)
                  ($wr-list obj col style output)
                  ($wr-list-pretty obj col style output)))))))

(define $wr-list-pretty
  (lambda (lst col style output)
    (let ((limit ($- 70 col)))
      (let ((str ($object->string lst (if ($< limit 0) 0 limit))))
        (if ($< ($string-length str) limit)
            ($out str col output)
            (let ((head ($car lst))
                  (rest ($cdr lst)))
              (if ($symbol? head)
                  (if (and ($pair? rest)
                           (or ($eq? head 'lambda)
                               ($eq? head 'let)
                               ($eq? head 'let*)
                               ($eq? head 'letrec)
                               ($eq? head 'define)
                               ($eq? head 'define-macro)
                               ($eq? head 'match)))
                      (let ((col2
                             ($wr head
                                  ($out "(" col output)
                                  style
                                  output)))
                        (let ((col3
                               ($wr ($car rest)
                                    ($out " " col2 output)
                                    style
                                    output)))
                          ($wr-list-aux
                           ($cdr rest)
                           col3
                           ($+ col 2)
                           style
                           output)))
                      (if ($<= ($string-length ($symbol->string head)) 5)
                          (let ((col4
                                 ($wr head
                                      ($out "(" col output)
                                      style
                                      output)))
                            ($wr-list-aux
                             rest
                             col4
                             (and col4 ($+ col4 1))
                             style
                             output))
                          ($wr-list lst col style output)))
                  ($wr-list lst col style output))))))))

(define $wr-list
  (lambda (lst col style output)
    (if ($pair? lst)
        ($wr-list-aux ($cdr lst)
                      (and col ($wr ($car lst)
                                    ($out "(" col output) style output))
                      (and col ($+ col 1))
                      style
                      output)
        ($out "()" col output))))

(define $wr-list-aux
  (lambda (lst col align style output)
    (if ($not col)
        col
        (if ($pair? lst)
            ($wr-list-aux ($cdr lst)
                          ($wr ($car lst)
                               (if ($eq? style 'pretty)
                                   ($indent align col output)
                                   ($out " " col output))
                               style
                               output)
                          align
                          style
                          output)
            (if ($null? lst)
                ($out ")"
                      col
                      output)
                ($out ")"
                      ($wr lst ($out " . " col output) style output)
                      output))))))

(define $wr-char
  (lambda (obj col style output)
    (if ($eq? style 'display)
	($out ($make-string 1 obj) col output)
	($out (if ($= ($char->integer obj) 32)
                  "space"
                  (if ($= ($char->integer obj) 10)
                      "newline"
                      ($make-string 1 obj)))
              ($out "#\\" col output)
              output))))

(define $wr-string
  (lambda (obj col style output)
    (if ($eq? style 'display)
        ($out obj col output)
        ($out "\""
              ($wr-string-aux obj 0 0 ($out "\"" col output) output)
              output))))

(define $wr-string-aux
  (lambda (str i j col output)
    (if ($< j ($string-length str))
        (let ((c ($string-ref str j)))
          (if (or ($char=? c #\")
                  ($char=? c #\\))
              ($wr-string-aux
               str
               j
               ($+ j 1)
               ($out "\\"
                     ($out ($substring str i j) col output)
                     output)
               output)
              (if ($char=? c #\newline)
                  ($wr-string-aux
                   str
                   ($+ j 1)
                   ($+ j 1)
                   ($out "\\n"
                         ($out ($substring str i j) col output)
                         output)
                   output)
                  ($wr-string-aux
                   str
                   i
                   ($+ j 1)
                   col
                   output))))
        ($out ($substring str i j) col output))))

(define $wr-vector
  (lambda (obj col style output)
    ($wr ($vector->list obj)
         ($out "#" col output)
         style
         output)))

(define $wr
  (lambda (obj col style output)
    (if ($pair? obj)
        ($wr-sexpr obj col style output)
        (if ($null? obj)
            ($wr-list obj col style output)
            (if ($char? obj)
                ($wr-char obj col style output)
                (if ($string? obj)
                    ($wr-string obj col style output)
                    (if ($vector? obj)
                        ($wr-vector obj col style output)
                        (if ($boolean? obj)
                            ($out (if obj "#t" "#f") col output)
                            (if ($number? obj)
                                ($out ($number->string obj) col output)
                                (if ($symbol? obj)
                                    ($out ($symbol->string obj) col output)
                                    (if ($procedure? obj)
                                        ($out "#<procedure>" col output)
                                        ($out "#<unknown>" col output))))))))))))

(define $object->string
  (lambda (obj limit)
    (let ((result ""))
      ($wr obj
           0
           'write
           (lambda (str)
             (set! result ($string-append result str))
             ($< ($string-length result) limit)))
      (if ($>= ($string-length result) limit)
          (begin
            (set! result ($substring result 0 limit))
            (if ($>= ($- limit 1) 0)
                (begin
                  ($string-set! result ($- limit 1) #\.)
                  (if ($>= ($- limit 2) 0)
                      (begin
                        ($string-set! result ($- limit 2) #\.)
                        (if ($>= ($- limit 3) 0)
                            ($string-set! result ($- limit 3) #\.))))))))
      result)))

(define $write-char
  (lambda (c)
    ($write-u8 ($char->integer c))))

(define $write-string
  (lambda (str)
    ($write-string-aux str 0)))

(define $write-string-aux
  (lambda (str i)
    (if ($< i ($string-length str))
        (begin
          ($write-char ($string-ref str i))
          ($write-string-aux str ($+ i 1)))
        #f)))

(define $newline
  (lambda ()
    ($write-char ($integer->char 10))))

(define $display
  (lambda (obj)
    ($wr obj 0 'display (lambda (str) ($write-string str) #t))))

(define $write
  (lambda (obj)
    ($wr obj 0 'write (lambda (str) ($write-string str) #t))))

(define $pretty-print
  (lambda (obj)
    ($wr obj 0 'pretty (lambda (str) ($write-string str) #t))
    ($newline)))

(define $println
  (lambda (obj)
    ($display obj)
    ($newline)))

(define test
  (lambda (obj)
    ($println "------------------")
    ($println obj)
    ($display obj)($newline)
    ($write obj)($newline)
    ($pretty-print obj)))


;; tests

(test 0)
(test 123)
(test -123)

(test #f)
(test #t)
(test '())

(test #\x)
(test #\ )
(test #\
)

(test "")
(test "abcd")
(test "ab\"cd")
(test "ab\ncd")

(test 'abcd)

(test '(1 2 3))
(test '(1 2 . 3))
(test '(quote (1 2 . 3)))

(test '#(1 2 3))

(test '(define (sort lst <?)
         (letrec ((mergesort
                   (lambda (lst)
                     (if (or (null? lst) (null? (cdr lst)))
                         lst
                         (let ((lst1 (mergesort (split lst))))
                           (let ((lst2 (mergesort (split (cdr lst)))))
                             (merge lst1 lst2))))))
                  (merge
                   (lambda (lst1 lst2)
                     (if (null? lst1)
                         lst2
                         (if (null? lst2)
                             lst1
                             (let ((e1 (car lst1)) (e2 (car lst2)))
                               (if (<? e1 e2)
                                   (cons e1 (merge (cdr lst1) lst2))
                                   (cons e2 (merge lst1 (cdr lst2)))))))))
                  (split
                   (lambda (lst)
                     (if (or (null? lst) (null? (cdr lst)))
                         lst
                         (cons (car lst) (split (cddr lst)))))))
           (mergesort lst))))

;------------------
;0
;0
;0
;0
;------------------
;123
;123
;123
;123
;------------------
;-123
;-123
;-123
;-123
;------------------
;#f
;#f
;#f
;#f
;------------------
;#t
;#t
;#t
;#t
;------------------
;()
;()
;()
;()
;------------------
;x
;x
;#\x
;#\x
;------------------
; 
; 
;#\space
;#\space
;------------------
;
;
;
;
;#\newline
;#\newline
;------------------
;
;
;""
;""
;------------------
;abcd
;abcd
;"abcd"
;"abcd"
;------------------
;ab"cd
;ab"cd
;"ab\"cd"
;"ab\"cd"
;------------------
;ab
;cd
;ab
;cd
;"ab\ncd"
;"ab\ncd"
;------------------
;abcd
;abcd
;abcd
;abcd
;------------------
;(1 2 3)
;(1 2 3)
;(1 2 3)
;(1 2 3)
;------------------
;(1 2 . 3)
;(1 2 . 3)
;(1 2 . 3)
;(1 2 . 3)
;------------------
;(quote (1 2 . 3))
;(quote (1 2 . 3))
;'(1 2 . 3)
;'(1 2 . 3)
;------------------
;#(1 2 3)
;#(1 2 3)
;#(1 2 3)
;#(1 2 3)
;------------------
;(define (sort lst <?) (letrec ((mergesort (lambda (lst) (if (or (null? lst) (null? (cdr lst))) lst (let ((lst1 (mergesort (split lst)))) (let ((lst2 (mergesort (split (cdr lst))))) (merge lst1 lst2)))))) (merge (lambda (lst1 lst2) (if (null? lst1) lst2 (if (null? lst2) lst1 (let ((e1 (car lst1)) (e2 (car lst2))) (if (<? e1 e2) (cons e1 (merge (cdr lst1) lst2)) (cons e2 (merge lst1 (cdr lst2))))))))) (split (lambda (lst) (if (or (null? lst) (null? (cdr lst))) lst (cons (car lst) (split (cddr lst))))))) (mergesort lst)))
;(define (sort lst <?) (letrec ((mergesort (lambda (lst) (if (or (null? lst) (null? (cdr lst))) lst (let ((lst1 (mergesort (split lst)))) (let ((lst2 (mergesort (split (cdr lst))))) (merge lst1 lst2)))))) (merge (lambda (lst1 lst2) (if (null? lst1) lst2 (if (null? lst2) lst1 (let ((e1 (car lst1)) (e2 (car lst2))) (if (<? e1 e2) (cons e1 (merge (cdr lst1) lst2)) (cons e2 (merge lst1 (cdr lst2))))))))) (split (lambda (lst) (if (or (null? lst) (null? (cdr lst))) lst (cons (car lst) (split (cddr lst))))))) (mergesort lst)))
;(define (sort lst <?) (letrec ((mergesort (lambda (lst) (if (or (null? lst) (null? (cdr lst))) lst (let ((lst1 (mergesort (split lst)))) (let ((lst2 (mergesort (split (cdr lst))))) (merge lst1 lst2)))))) (merge (lambda (lst1 lst2) (if (null? lst1) lst2 (if (null? lst2) lst1 (let ((e1 (car lst1)) (e2 (car lst2))) (if (<? e1 e2) (cons e1 (merge (cdr lst1) lst2)) (cons e2 (merge lst1 (cdr lst2))))))))) (split (lambda (lst) (if (or (null? lst) (null? (cdr lst))) lst (cons (car lst) (split (cddr lst))))))) (mergesort lst)))
;(define (sort lst <?)
;  (letrec ((mergesort
;            (lambda (lst)
;              (if (or (null? lst) (null? (cdr lst)))
;                  lst
;                  (let ((lst1 (mergesort (split lst))))
;                    (let ((lst2 (mergesort (split (cdr lst)))))
;                      (merge lst1 lst2))))))
;           (merge (lambda (lst1 lst2)
;                    (if (null? lst1)
;                        lst2
;                        (if (null? lst2)
;                            lst1
;                            (let ((e1 (car lst1)) (e2 (car lst2)))
;                              (if (<? e1 e2)
;                                  (cons e1 (merge (cdr lst1) lst2))
;                                  (cons e2 (merge lst1 (cdr lst2)))))))))
;           (split (lambda (lst)
;                    (if (or (null? lst) (null? (cdr lst)))
;                        lst
;                        (cons (car lst) (split (cddr lst)))))))
;    (mergesort lst)))
