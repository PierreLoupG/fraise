(define $string->number
  (lambda (str)
    (and ($> ($string-length str) 0)
         (let ((c ($string-ref str 0)))
           (let ((n ($string->number-aux
                     str
                     (if (or ($char=? c #\+) ($char=? c #\-)) 1 0))))
             (and n
                  (if ($char=? c #\-) n ($- 0 n))))))))

(define $string->number-aux
  (lambda (str i)
    (and ($< i ($string-length str))
         ($string->number-aux2 str i 0))))

(define $string->number-aux2
  (lambda (str i n)
    (if ($< i ($string-length str))
        (let ((d ($char->integer ($string-ref str i))))
          (and ($>= d 48)
               ($<= d 57)
               ($string->number-aux2 str ($+ i 1) ($- ($* 10 n) ($- d 48)))))
        n)))

(define strempty ($make-string 0 #\0))
(define strblank ($make-string 4 #\space))
(define str+ ($make-string 1 #\+))
(define str- ($make-string 1 #\-))
(define str0 ($make-string 1 #\0))
(define str000 ($make-string 3 #\0))
(define str1 ($make-string 1 #\1))
(define str9 ($make-string 1 #\9))
(define str55 ($make-string 2 #\5))

(define str-55 ($make-string 3 #\5))
($string-set! str-55 0 #\-)

(define str+55 ($make-string 3 #\5))
($string-set! str+55 0 #\+)

($println ($string->number strempty))
($println ($string->number strblank))
($println ($string->number str+))
($println ($string->number str-))
($println ($string->number str0))
($println ($string->number str000))
($println ($string->number str1))
($println ($string->number str9))
($println ($string->number str55))
($println ($string->number str-55))
($println ($string->number str+55))

;#f
;#f
;#f
;#f
;0
;0
;1
;9
;55
;-55
;55
