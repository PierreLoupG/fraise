(define f
	(lambda (n)
		(lambda (x)
			(println n)
			(println ($+ n n))
			(let ((y 1))
				(println #\Z)
				(println ($+ y n)))
			(println #\a)
			($+ n x))))

(define g (f 4))
(println (g 10))

;4
;8
;Z
;5
;a
;14
