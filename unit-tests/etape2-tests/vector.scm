(define v ($make-vector 6 2))
(println (vector-length v))
(println (vector-ref v 2))
(vector-set! v 2 4)
(println (vector-ref v 2))
(println v)

;6
;2
;4
;#(224222)
