(define f
	(lambda (n)
		(g (lambda (x)
				(+ n x))
			(lambda (y)
				(println n)
				(set! n y)
				(println (* n 2))))))
			
(define g
	(lambda (read-n write-n)
		(write-n (read-n 1))
		(read-n 0)))

(println (f 42))

;42
;86
;43
