(define (loop n)
	(if ($<= n 0)
		(println n)
		(begin
			(println n)
			(loop ($- n 1)))))
		
(loop 4)

;4
;3
;2
;1
;0
