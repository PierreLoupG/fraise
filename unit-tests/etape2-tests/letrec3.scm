(define f 4)
(define g 6)

(define (even? n)
	(letrec ((f (lambda (n) (if ($= n 0) #t (g ($- n 1)))))
					(g (lambda (n) (if ($= n 0) #f (f ($- n 1))))))
		(f n)))

(println (even? 5))

(println g)

;#f
;6
