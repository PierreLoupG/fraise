(define p (delay (println "Mais délivrez nous des bugs")))
(println "Que la compilation soit faite Just-in-Time et Ahead-of-Time,")
(println "......")
(println "Ne nous soumettez pas à des test unitaires trop difficiles")
(println p)
(force p)

;Que la compilation soit faite Just-in-Time et Ahead-of-Time,
;......
;Ne nous soumettez pas à des test unitaires trop difficiles
;<procedure>
;Mais délivrez nous des bugs
