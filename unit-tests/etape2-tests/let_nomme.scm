(println (let fact ((n 5))
	(if ($= n 0)
		1
		($* n (fact ($- n 1))))))

;120
