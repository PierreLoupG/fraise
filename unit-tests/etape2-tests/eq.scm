(println (eq? 1 1))
(println (eq? 1 2))
(define a 1) 
(define b 1)
(println (eq? a 1))
(println (eq? a b))
(define v (make-vector 2 1))
(define x (make-vector 2 1))
(println (eq? v x))
(define v x)
(println (eq? v x))

;#t
;#f
;#t
;#t
;#f
;#t
