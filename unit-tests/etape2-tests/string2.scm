(define s ($make-string ($+ 3 3) #\a))
(println ($string-length s))
(println ($string-ref s 2))
($string-set! s 2 #\b)
(println ($string-ref s 2))
(println s)

;6
;a
;b
;aabaaa
