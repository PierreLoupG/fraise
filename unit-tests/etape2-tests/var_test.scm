(define f
	(lambda (n)
		(lambda (x)
				(set! x ($+ x n))
				(println x))))
			
(define g (f 5))
(println (g 12))

;17
;#f
