(letrec ((loop (lambda (n)
									(println n)
									(if ($<= n 0)
										'()
										(loop ($- n 1))))))
			(loop 3))		

;3
;2
;1
;0
