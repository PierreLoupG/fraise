(define f
	(lambda (a b c)
		(lambda (x y z)
			($+ a y))))
			
(define g (f 1 2 3))
(println (g 20 10 0))

;11
