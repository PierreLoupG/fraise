# File: makefile

.SUFFIXES:
.SUFFIXES: .scm .s .o .exe .pdf .tex .c

all: rts.o mmap.o gc.o

.s.o:
	gcc -c -o $*.o $*.s

.c.o:
	gcc -c -o $*.o $*.c

ut: all
	./run-unit-tests.scm

clean:
	rm -f *.o *.exe *~ *.log *.aux unit-tests/*.s unit-tests/*.ir unit-tests/*.exe unit-tests/*~ unit-tests/*/*.s unit-tests/*/*.exe unit-tests/*/*.ir

rapport: etape3.pdf 

etape3.pdf: etape3.tex
	pdflatex etape3.tex
