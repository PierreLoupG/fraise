#include <stdio.h>

typedef long long int long_word;

#ifdef __linux__
#define stack_base _stack_base
#define stack_ptr _stack_ptr
#define from_space _from_space
#define to_space _to_space
#define glo_base _glo_base
#define glo_ptr _glo_ptr
#define alloc_ptr _alloc_ptr
#define gc _gc
#endif

#define BH 57

/* variables qui seront mises à jour avant d’appeler gc() */
long_word *stack_base, *stack_ptr, *from_space, *to_space, *glo_base, *glo_ptr; /* les pointeurs de la pile, des variables globales */
long_word *alloc_ptr, *scan, *copy;                                             /* pointeur pour la copie vers le to_space */
/* fonctions outils */
int isref(long_word val) { return (2 < (val & 7)); }                            /* retourne si la valeur est une référence vers le tas */
int isbh(long_word val) { return (val == BH); }                                 /* retourne si la valeur est BH, i.e. déjà copiée 111001 = 💔️ */
long_word gtag(long_word ptr) {	return (long_word) (ptr & 7); }                 /* retourne le tag du pointeur */
long_word *rtag(long_word ptr) { return (long_word *) (ptr & -8); }             /* retourne le pointeur sans son tag */
long_word tsadr(long_word *base, long_word *ptr) {                              /* retourne l'adresse dans le to_space avec le tag */
	return ((long_word) base) + gtag(*ptr); 
}
/* copie un élément du tas du from_space vers le to_space */
void move(long_word *from, long_word size) {
    long_word* realadr = rtag(*from); long_word newadr = tsadr(copy, from);     /* adresse from_space sans tag, adresse to_space + tag */
    for (int i = 0; i < size; i++) { *copy = *(realadr + i); copy++;}           /* copie de l'élément vers le tospace */
    *realadr = BH; *(realadr + 1) = newadr;                                     /* from_space_adr = 💔️ , from_space_adr+1 = fowarding pointer*/
    *from = newadr;                                                             /* mise à jour de la référence à l'objet copié */
}
/* sous-routine du glaneur de cellules */
int gcrt(long_word* adr){	                                                    	
    if (isref(*adr)) {                                                          /* si c'est une référence : */		
        if (isbh(*(rtag(*adr)))) *adr = *(rtag(*adr) + 1);                      /* si 💔️, racine mise à jour à nouvelle adresse */
        else {                                                                  
            if (gtag(*adr) == 3) move(adr, 2);                                  /* si paire, élément déplacé dans to_space avec taille de 2*/
            else move(adr, *(rtag(*adr))/8 + 1);                                /* sinon, structure de longueur n avec n+1 champs */
            return 1;                                                           /* si copie, retourne 1 pour indiquer que scan nécessaire */
        }
    }return 0;                                                                  /* aucune copie effectuée; on peut omettre la phase scan */	
}
/* le glaneur de cellules */
void gc() {    
    printf("\n*** début du GC\n mémoire occupée %ld bytes\n stack size %ld\n global variables: %ld\n global base: %p\n", 8*(alloc_ptr - from_space), (stack_base - stack_ptr), (glo_base - (glo_ptr + 1)), glo_base);
    copy = to_space; scan = to_space; long_word* root;                          /* initialization de scan et copy */
    for (root = stack_ptr; root < stack_base; root += 1)                        /* passage sur la pile */
	    if(gcrt(root)) while(scan < copy) {gcrt(scan); scan++;}
    
    if (glo_base != 0) {for (root = glo_base; root >= glo_ptr; root -= 1)      /* passage sur les variables globales (s'il y en a) */
        if(gcrt(root)) while(scan < copy) {gcrt(scan); scan++;}} 
    printf("*** fin du GC\n mémoire occupée %ld bytes\n stack size: %ld\n global variables: %ld\n", 8*(scan - to_space), (stack_base - stack_ptr), (glo_base - (glo_ptr + 1)));
    long_word *t = to_space; to_space = from_space; from_space = t;             /* inverse to_space et from_space */
    alloc_ptr = copy;                                                           /* mise à jour du pointeur d'allocation */
}