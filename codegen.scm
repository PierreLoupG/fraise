;;; File: codegen.scm

;;;============================================================================

;; Compiler from AST to IR.

;; Note: an S-expression is an AST of sorts!

;; liste du code scheme de toute les lambdas rencontrées
;; lors de la compilation ainsi que son environnement
(define lam '())
;; liste des variables globales rencontrées lors de la compilation
(define list-glo '())

;; (create-lambda env proc expr)
;; ajoute à la liste des lambdas à compiler une nouvelle lambda
;; avec :
;; 'env' son environnement lors de sa rencontre
;; 'proc' les IR pour créer la lambda
;; 'expr' le code scheme de la lambda
(define (create-lambda env proc expr)
	(begin
		;; la lambda appelante ayant l'environnement le plus à jour
		;; on récupère ses globales comme étant celle de tout le programme
		(set! list-glo (env-get-glo env))
		;; les données sont ajoutées à la fin étant donné qu'une lambda
		;; peut générer un appel à une lambda, voir '(compile-lambda)'
		(set! lam (reverse (cons (cons env (cons proc expr)) (reverse lam))))))
	
;; (compile-lambda)
;; compile toutes les lambdas tant qu'il en reste dans la liste
(define (compile-lambdas)
	(let ((ir '()))
		(let while ()
			(if (null? lam)
				ir
				(begin
					(set! ir (cons
						(append 
							(cadar lam) ; 'proc' : l'appel à la procédure lambda
							(car (compile-expr (cddar lam) (union-glo (caar lam) list-glo) 'tail)) ; corps du lambda
						;`((ret 1))
						    ) ir))
					(set! lam (list-tail lam 1))
					(while))))))

;; (compile-program exprs) 
;; returns the list of IR instructions that
;; correspond to the sequence of expressions "exprs".  For example:
;;
;; (compile-program '( (println 1) (println (+ 2 3)) )) =>
;;
;;    ( (push_lit 1) (println) (push_lit 2) (push_lit 3) (add) (println) )
(define (compile-program exprs)
	(append 
		(concatenate (map compile-top exprs)) 
		`((exit)) 
		(concatenate (compile-lambdas))))

;; (compile-top expr)
;; compile l'expression avec un envrionnement vide
(define (compile-top expr)
	(let ((result-data (compile-expr expr '(0) 'push)))
		(append (car result-data) `((move 0 1)))))

;; (compile-exprs exprs env ir)
;; compile plusieurs expressions à la suite
(define (compile-exprs exprs env ir)
	(if (null? exprs)
		(cons ir env)
		(let ((d (compile-expr (car exprs) env 'push)))
			(compile-exprs
				(cdr exprs)
				(cdr d)
				(append ir (car d))))))
			
;; (expand-match expr) et ses fonctions auxilliaires
;;  transformation source à source sur la forme (match expr . clauses)

;;;============================================================================
;; (expand-match-if-equal?)
;; construit le code responsable de tester la correspondance entre les clauses
;; et l'expression fournie à la macro match

(define expand-match-if-equal?
  (lambda (expr var pattern yes no)
    (if (and (pair? pattern)
             (eq? (car pattern) 'unquote)
             (pair? (cdr pattern))
             (null? (cdr (cdr pattern))))
        (cons 'let
               (cons (cons (cons (car (cdr pattern))
                                    (cons var
                                           '()))
                             '())
                      (cons yes
                             '())))
        (if (or (null? pattern)
                (symbol? pattern)
                (boolean? pattern)
                (number? pattern)
                (char? pattern)
                (string? pattern))
            (cons 'if
                   (cons (cons 'equal?
                                 (cons var
                                        (cons (cons 'quote
                                                      (cons pattern
                                                             '()))
                                               '())))
                          (cons yes
                                 (cons no
                                        '()))))
            (if (pair? pattern)
                (let ((carvar (gensym))
                      (cdrvar (gensym)))
                  (cons
                   'if
                   (cons
                    (cons 'pair?
                           (cons var
                                  '()))
                    (cons
                     (cons
                      'let
                      (cons
                       (cons
                        (cons carvar
                               (cons (cons 'car
                                             (cons var
                                                    '()))
                                      '()))
                        '())
                       (cons
                        (expand-match-if-equal?
                         expr
                         carvar
                         (car pattern)
                         (cons 'let
                                (cons (cons (cons cdrvar
                                                     (cons (cons 'cdr
                                                                   (cons var
                                                                          '()))
                                                            '()))
                                              '())
                                       (cons (expand-match-if-equal?
                                               expr
                                               cdrvar
                                               (cdr pattern)
                                               yes
                                               no)
                                              '())))
                         no)
                        '())))
                     (cons no
                            '())))))
                (error "SYNTAX ERROR ON EXPRESSION: " expr))))))

;;;============================================================================
;; expand-match-clauses
;; fonction auxilliaire construisant les clauses du match

(define expand-match-clauses
  (lambda (expr var fns1 fns2 clauses body)
    (if (pair? clauses)
        (let ((clause (car clauses)))
          (expand-match-clauses
           expr
           var
           (cdr fns1)
           (cdr fns2)
           (cdr clauses)
           (cons
            'let
            (cons
             (cons
              (cons
               (car fns1)
               (cons
                (cons
                 'lambda
                 (cons
                  (cons
                   var
                   '())
                  (cons
                   (expand-match-if-equal?
                    expr
                    var
                    (car clause)
                    (if (and (eq? (car (cdr clause)) 'when)
                             (pair? (cdr (cdr clause))))
                        (cons 'if
                               (cons (car (cdr (cdr clause)))
                                      (cons (car (cdr (cdr (cdr clause))))
                                             (cons (cons (car fns2)
                                                           (cons var
                                                                  '()))
                                                    '()))))
                        (car (cdr clause)))
                    (cons (car fns2)
                           (cons var
                                  '())))
                   '())))
                '()))
              '())
             (cons body
                    '())))))
        body)))

;;;============================================================================
;; expand-match, la fonction principale

(define expand-match
  (lambda (expr)
    (let ((subject
           (car (cdr expr)))
          (clauses
           (cdr (cdr expr)))
          (var
           (gensym))
          (err
           (gensym)))
      (let ((fns
             (append (map (lambda (x) (gensym))
                            clauses)
                      (cons err
                             '()))))
        (cons
         'let
         (cons
          (cons (cons var
                        (cons subject
                               '()))
                 '())
          (cons
           (cons
            'let
            (cons
             (cons
              (cons err
                     (cons (cons 'lambda
                                   (cons (cons var
                                                 '())
                                          (cons (cons 'match-failed
                                                        (cons var
                                                               '()))
                                                 '())))
                            '()))
              '())
             (cons (expand-match-clauses
                     expr
                     var
                     fns
                     (cdr fns)
                     clauses
                     (cons (car fns)
                            (cons var
                                   '())))
                    '())))
           '())))))))
	
	

	
;; (compile-expr expr data)
;; créer les IR correspondant à 
;; l'expressions "expr" en considérant l'envrionnement "env"
;; retourne une paire des IR créées et du nouvel environnement
;; sous la forme (IR . (SP . ENV)) où SP est le stack pointer
;; de l'environnement ENV. Par exemple :
;;
;; (compile-expr '(println (+ a 3)) (1 (a . 1))) =>
;;
;;    (((push_loc 1) (push_lit 3) (add) (println)) (2 (a . 1)))

(define (compile-expr expr env context)	
	
  (define (send)
    (if (eq? context 'tail)
        (begin
          ;(printlog "RET" env)
          ;(println "--> RET " (+ (env-retaddr-pos env) 0) " " (env-cs-pos env))
          `((ret ,(+ (env-retaddr-pos env) 0) ,(- (env-cs-pos env) 1))))
        `()))
        
  ;; affichage pour le débugage
  ;(printcpl expr env context)
        
  (match expr
	

    ;((println ,expr)  	
      ;(make-data* 'println env expr))
	
	;;;============================================================================
	;; delay
	
    ((delay ,expr)
        (let ((v (gensym)) (f? (gensym)) (r (gensym)))
            (compile-expr `(let ((,f? #f) (,v #f))
		                        (lambda ()
			                        (if ,f?
		    	                        ,v
			    	                    (let ((,r ,expr))
			    		                    (set! ,f? #t)
			    		                    (set! ,v ,r)
			    		                    ,v)))) env context)))
    
    	
	;;;============================================================================
    ;; match
    
    ((match ,subject . ,clauses)
       (compile-expr (expand-match expr) env context))
     		
    ;;;============================================================================
	;; données
  
		;; nombre
    (,val when (number? val)
   		(begin
   			(env-sp+ env)
     		(make-data env `((push_lit ,(* val 8)) ,@(send)))))
     		
	;; boolean
	(,val when (boolean? val)
		(begin
			(env-sp+ env)
			(make-data env (if (eq? #t val) `((push_lit 9) ,@(send)) `((push_lit 1) ,@(send))))))
		
		;; caractére
		(,val when (char? val)
			(begin
				(env-sp+ env)
				(make-data env `((push_lit ,(+ (* (char->integer val) 8) 2)) ,@(send)))))
    
		;; string
		(,val when (string? val) 
			(let ((str (gensym)))
				(compile-expr 
					`(let ((,str ($make-string ,(string-length val) #\space)))
		      	,@(map (lambda (i) `($string-set! ,str ,i ,(string-ref val i))) (iota (string-length val)))
		        ,str)
		      env context)))
    
		;; constante liste vide
		((quote ,expr) when (eq? expr (quote ()))
		  (begin
		    (env-sp+ env)
			  (make-data env `((push_lit 25) ,@(send)))))
		
		;; constante
		((quote ,expr)  	
			(if (or (number? expr) (boolean? expr) (char? expr))
			    (compile-expr expr env context)
			    (begin
				    (gen-const expr)
				    (let*	((gen-data (compile-exprs (reverse const-code) (list-copy env) '()))
				    			(const-data (compile-expr (cdr (assq expr const-table)) (dup-env gen-data) context)))
				    	      (begin*
				    	      	(make-data env
				    	      			(car gen-data)
				    	      			(car const-data)
				    	      			(if (eq? context 'push) `((move 1 ,(length const-code))) (send))) ; dealloc du nombre de constante crée
				    	      	(env-sp+ env)
				    	      	(set! const-code '())   
		    					(set! const-table '()))))))
	
		;variable
		(,var when (symbol? var)
			(let* ((v (env?var env var))
						 (a (env?assig env var)) 
						 (u (cond
									((and (eq? v #f) (eq? a #f)) #f)
									((and (eq? v #f) a) a)
									((and (eq? a #f) v) v) 
									((and v a (glo? v)) a)
									((< (list-pos (cdr env) v) (list-pos (cdr env) a)) v)
									(else a))))
				(if (eq? u #f)
					(error "variable inconnue:" var))
					(if (and (not (assig? var)) (assig? u))
						(compile-expr `($car ,(string->symbol (car u))) env context)
							(cond
								((free? u)
									(let ((c-pos (env-close-pos env)) (var-pos (list-pos (env-get-free env) var)))
									    (begin*
									        (make-data env 
										        (list (list 'push_loc c-pos))
										        (list (list 'push_free var-pos))
										        (send)) 
										        (env-sp+ env))))
								((glo? u) ;;l'incrémentation du sp est exécutée avant pour que le send ait les bonnes valeurs
									(begin (env-sp+ env) (make-data env (list (list 'push_glo (car u))) (send))))
								(else
									(begin* (make-data env (list (list 'push_loc (env-var-pos env u))) (send)) (env-sp+ env)))))))
							
	;;;============================================================================
	;; tests

		(($number? ,expr)
			(make-data* 'is_num env context expr))

		(($char? ,expr)
			(make-data* 'is_char env context expr))

		(($string? ,expr)
			(make-data* 'is_str env context expr))

		(($vector? ,expr)
			(make-data* 'is_vec env context expr))

		(($symbol? ,expr)
			(make-data* 'is_sym env context expr))

		(($pair? ,expr)
			(make-data* 'is_pair env context expr))

		(($procedure? ,expr)
			(make-data* 'is_proc env context expr))	

		(($eq? ,expr1 ,expr2)
			(make-data* 'is_eq env context expr1 expr2))
		
	;;;============================================================================
	;; vecteur	

	(($make-vector ,size ,content)
		(make-data* 'make_vec env context size content))
    
    (($vector-ref ,vec ,index)
    	(make-data* 'vec_ref env context vec index))
    	
    (($vector-set! ,vec ,index ,value)
    	(make-data* 'vec_set env context vec index value))
    
    (($vector-length ,vec)
    	(make-data* 'vec_lgt env context vec))

	;;;============================================================================
	;; string    	
    	
	(($make-string ,size ,content)
		(make-data* 'make_str env context size content))
    
    (($string-ref ,str ,index)
    	(make-data* 'str_ref env context str index))
    	
    (($string-set! ,str ,index ,value)
    	(make-data* 'str_set env context str index value))
    
    (($string-length ,str)
    	(make-data* 'str_lgt env context str))

	;;;============================================================================
	;; pair
	    	
    (($cons ,expr1 ,expr2)
    	(make-data* 'make_cons env context expr1 expr2))
    	
    (($car ,pair)
    	(make-data* 'cons_car env context pair))
    
    (($cdr ,pair)
    	(make-data* 'cons_cdr env context pair))
    
    (($set-car! ,pair ,value)
    	(make-data* 'set_car env context pair value))
    
    (($set-cdr! ,pair ,value)
    	(make-data* 'set_cdr env context pair value))
  
	;;;============================================================================
	;; set
	   
   	;; set! pour variable libre dans lambda se transformant en set-car!
   	((set! ,var ,expr) when (free? (env?var env var))
    	(compile-expr `($set-car! ,(prefix "#f_" var) ,expr) env context))
    
    ((set! ,var ,expr) when (env?assig env var)
    	(compile-expr `($set-car! ,(prefix "#f_" var) ,expr) env context))
    
    ;; set! pour variable globale qui est juste la mise à jour le de variable (pop_glo)
    ((set! ,var ,expr) when (glo? (env?var env var))
     	(make-data* `((pop_glo ,var)) env context expr))
   	
   	;; set! pour variable locale	
   	((set! ,var ,expr)
   		(let ((p (env-var-pos env (env?var env var))))
   			(make-data* `((set ,p)) env context expr)))

	;;;============================================================================
	;; I/O   	
   	  		
  	(($write-u8 ,expr)
  		(make-data* 'write_u8 env context expr))

    (($read-u8)
    	(begin
    	(env-sp+ env)
    		(make-data env `((read_u8) ,@(send)))))
  
 	;;;============================================================================
	;; begin
    
    ;; begin simple
    ((begin ,expr)
    	(compile-expr expr env context))
    
    ;; begin multiple
    ((begin . ,exprs)
	  	(let* ((rev (reverse exprs))
	  				(last (car rev))
	  				(firsts (map genvar (reverse (cdr rev)))))
	  		(compile-expr 
	  			`(let ,firsts ,last) env context)))
    
	;;;============================================================================
	;; define
    
    ;; define fonction
   	((define (,name . ,formal) . ,expr)
   		(compile-expr `(define ,name (lambda (,@formal) ,@expr)) env context))

		;; define variable
    ((define ,name . ,expr)
    	(let* ((e (env+glo env name)) (d (compile-expr `(begin ,@expr) env context)))
		  		(make-data e (car d) `((pop_glo ,name)))))

	;;;============================================================================
	;; lambda

    ;; lambda fonction
    (((lambda ,vars . ,expr) . ,args)
   		(let ((c (gensym)))
	    	(compile-expr `(let ((,c (lambda ,vars ,@expr))) (,c ,@args)) env context)))
   	
   	;; lambda
    ((lambda ,vars . ,expr)
  		(let*
  			;; créer les instructions pour la fermeture à partir de l'env
  			((close-ir (env-close env))
			  (label (gensym))
			  ;; data à renvoyer : instructions pour l'appel du lambda
  			(data (make-data env `((push_proc ,label)) close-ir (send)))
  			;; env du lambda : ajout des arguments et met libre les anciennes variables
  			(env-lam (env+vars (env+cs (env-set-free env)) vars))
  			;; récupère les assignations
  			(assig (append (get-env-assig env-lam) (get-assig vars))))
  				;; ajoute un point de repère pour la fermeture (#f)
					(env+close env-lam)
					(env+retaddr env-lam)
					(create-lambda
						env-lam
						`((proc ,label ,(length vars)))
						`(let ,assig ,@expr))
					data))
 
	;;;============================================================================
	;; opération arithmétique  

    (($+ ,expr1 ,expr2)
    	(make-data* 'add env context expr1 expr2))

    (($- ,expr1 ,expr2)
    	(make-data* 'sub env context expr1 expr2))

    (($* ,expr1 ,expr2)
    	(make-data* 'mul env context expr1 expr2))

    (($quotient ,expr1 ,expr2)
    	(make-data* 'quotient env context expr1 expr2))
    	
    (($remainder ,expr1 ,expr2)
    	(make-data* 'remainder env context expr1 expr2))
    	
    (($modulo ,expr1 ,expr2)
    	(make-data* 'modulo env context expr1 expr2))

	;;;============================================================================
	;; opération de test   
    	
		(($= ,expr1 ,expr2)
			(make-data* 'equal env context expr1 expr2))
								
		(($< ,expr1 ,expr2)
			(make-data* 'inf env context expr1 expr2))

		(($<= ,expr1 ,expr2)
			(compile-expr `($not ($< ,expr2 ,expr1)) env context))

		(($> ,expr1 ,expr2)
			(compile-expr `($< ,expr2 ,expr1) env context))

		(($>= ,expr1 ,expr2)
			(compile-expr `($not ($> ,expr2 ,expr1)) env context))
		
	;;;============================================================================
	;; let		
		
		;; let nommé
		((let ,name ,vars . ,body) when (not (list? name))
			(compile-expr
				`(letrec ((,name (lambda ,(map cars vars) ,@body)))
									(,name ,@(map cdrs vars))) env context))
		
		;; let avec define fonction
		((let ,vars (define (,name . ,formal) . ,dexpr) . ,exprs)
			(compile-expr `(let ,vars (let ((,name (lambda (,@formal) ,@dexpr))) ,@exprs)) env context))
				
		;; let avec define variable
		((let ,vars (define . ,dexprs) . ,exprs)
			(compile-expr `(let ,vars (let (,dexprs) ,@exprs)) env context))
		
		;; let
		((let ,vars . ,expr)
			(let
				;; env temporaire pour la création des vars
				((env-tmp (list-copy env))
				;; env vide pour le corps, contiendra les data des variables créées
				(data-let (cons '() (list-copy env))))
				(set! vars (get-let-assig vars))
					(let loop ((v vars))
							(if (null? v)
								(let ((env (env-sp+ env))
										 (expr-ir (car (compile-expr `(begin ,@expr) (dup-env data-let) context))))
											;; retourne l'env et les IR résultantes du let
											(make-data env (car data-let) expr-ir (if (eq? 'push context) `((move 1 ,(length vars))) (send))))
								(begin
									;; compile la variable avec l'env temporaire
									(let* ((ec (list-copy env-tmp)) (d (compile-expr (cadar v) env-tmp 'push)))
										(set! env-tmp (list-copy ec))
										(env-sp+ env-tmp)
										(env+var (cdr d) (caar v))
										;; l'ajoute à l'env du let
										(data-merge data-let d))
									(loop (cdr v)))))))

		;; let*
		((let* ,vars . ,expr)
			(if (null? vars)
				(compile-expr `(begin ,@expr) env context)
				(let ((v (car vars)) (vn (cdr vars)))
					(compile-expr `(let (,v) (let* ,vn ,@expr)) env context))))
					
		;; letrec
		((letrec ,bindings . ,body)
			(compile-expr (letrec-gen bindings body) env context))
			
    ;;;============================================================================
	;; branchement if			
	
		;; if a une branche
	  ((if ,test ,expr)
		  (compile-expr `(if ,test ,expr #f) env context))

    ;; if		
    ((if ,expr1 ,expr2 ,expr3)
    	(let ((else (cons "else" (gensym)))	(end (cons "end" (gensym))))
    		(let* ((d1 (compile-expr expr1 (list-copy env) 'push))
    		      (t (dup-env d1))
    		      (e t)
    		      (noTC (eq? 'push context))
    					(d2 (compile-expr expr2 (list-copy e) context))
    					(d3 (compile-expr expr3 (list-copy e) context)))
								(cons 
									(append
										(car d1)
										`((jif ,else))
										(car d2)
										(if noTC `((jl ,end)) '())
										`((label ,else))
										(car d3)
										(if noTC `((label ,end) (move 1 1)) '()))
										(cdr d1)))))

	;;;============================================================================
	;; opération									

	(($not ,expr)
		(compile-expr `(if ,expr #f #t) env context))

	((and ,expr)
		(compile-expr expr env context))

	((and ,expr1 . ,exprs)
		(compile-expr `(if ,expr1 (and ,@exprs) #f) env context))		

	((or ,expr)
		(compile-expr expr env context))

	;; or				
	((or ,expr1 . ,exprs)
		(compile-expr
			(let ((e1 (gensym)))
				`(let ((,e1 ,expr1)) (if ,e1 ,e1 (or ,@exprs))))
			env context))

	;;;============================================================================
	;; cond
					
	((cond) 
	    (compile-expr #f env context))

    ((cond (else ,e1 . ,es))
        (compile-expr `(begin ,e1 ,@es) env context))
        
    ((cond (else . ,es) . ,rest)
        (error "compile-expr encountered an improper else clause at " isntr))
        
    ((cond (,test) . ,rest)
        (compile-expr `(or ,test (cond ,@rest) env context)))
        
    ((cond (,test => ,fn) . ,rest)
        (let ((v (gensym)))
            (compile-expr `(let ((,v ,test))
                                (if ,v
                                    (,fn ,v)
                                    (cond ,@rest))) env context)))
                    
    ((cond (,test => . ,es) . ,rest)
        (error "compile-expr encountered an improper => clause at " instr))
        
    ((cond (,test ,e1 . ,es) . ,rest)
        (compile-expr `(if ,test
                            (begin ,e1 ,@es)
                            (cond ,@rest)) env context))
				
	;;;============================================================================
	;; conversion
					
    (($integer->char ,expr)
    	(make-data* 'i2c env context expr))
    	
    (($char->integer ,expr)
    	(make-data* 'c2i env context expr))
    	
    (($symbol->string ,expr)
    	(make-data* 'sym2str env context expr))
    
    (($string->symbolaux ,expr)
	    (make-data* 'str2sym env context expr))
	    
	;;;============================================================================
	;; procédure

	;; appel de procédure
	((,proc . ,args)
	  	(let*
	  		((data-args (compile-exprs args (list-copy env) '()))
	  		(data-call (compile-expr proc (dup-env data-args) 'push))
	  		(nargs (length args))
	  		(lab (gensym))
	  		(tC (eq? context 'tail))
	  		(call-instr (if tC
	  	                (begin 
	  		                ;(print "--> MOVE " (+ 2 nargs) " " old-local-env-size " ON : ")
	  		                ;(printlog "MOVE" (cdr data-call))
	  		                ;(display (cdr (dup-env data-call))) (println "")
  					        `((push_loc ,(env-retaddr-pos (dup-env data-call)))
  					         (move ,(+ 2 nargs) ,(- (env-cs-pos (dup-env data-call)) (+ nargs 1))))) ;;nargs+2=retaddr+self+args
  					        `((push_ra ,lab))))) ;;si appel non terminal, créer adresse de retour
	  		  ;(println "--> SIZE " old-local-env-size)
					(begin*
	  				(make-data env
	  					(car data-args)
	  					(car data-call)
	  					call-instr
	  					(cons `(jump ,nargs) (if tC '() `((retaddr ,lab)))))
	  				(env-sp+ env))))

	;;;============================================================================
	;; autre  			
            	  
    ;; erreur
    (_
     (error "compile-expr encountered the unknown instruction" instr))))

